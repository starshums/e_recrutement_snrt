$(document).ready(function () {
    var dd;
    var coun=0, coun1=0,coun2=0,coun3=0,coun4=0,coun5=0;
    var count_exp = 0;
    $("#add_experience").on("click", function() {
        if( count_exp >= 5 ) {
            
        } else {
            count_exp++;
            $("#experience-content").append('<h3 class="p-1 exp_style sli'+ count_exp+' text-center"> Experience N°' + count_exp + ' <i class="fas fa-window-close float-right p-1 closed_' + count_exp + '"></i></h3><div class="form-experience_' + count_exp + '"><div class=""><label for="poste_' + count_exp + '" >Poste occupe</label><b style="color:#F00"> *</b><input id="poste_' + count_exp + '" class="form-control" required="true"  type="text" name="poste_' + count_exp + '" /> </div> <div class=""><label for="salaire_' + count_exp + '">Salaire</label><b style="color:#F00"> *</b><input id="salaire_' + count_exp + '" class="form-control" required="true" name="salaire_' + count_exp + '"  /></div> <div class=""><label for="employeur_' + count_exp + '">Employeur</label><b style="color:#F00"> *</b><input id="employeur_' + count_exp + '" class="form-control" required="true" name="employeur_' + count_exp + '"  /></div> <div class=""><label for="secteur_' + count_exp + '">Secteur</label><b style="color:#F00"> *</b><input id="secteur_' + count_exp + '" class="form-control" size="60" maxlength="128" required="true" name="secteur_' + count_exp + '"  /></div><div class="form-row"><div class="form-group col-sm-6"><label for="date_debut_' + count_exp + '">Date Début</label><b style="color:#F00"> *</b> <input class="form-control datepicker date_deb"     id="date_debut_' + count_exp + '" name="date_debut_' + count_exp + '" required /></div> <div class="form-group col-sm-6"><label for="date_fin_' + count_exp + '">Date Fin</label><b style="color:#F00"> *</b> <input class="form-control datepicker date_f"    id="date_fin_' + count_exp + '" name="date_fin_' + count_exp + '"   required  /></div> </div></div>');
            //$("#experience-content").append('<hr><!--<h6 style="color:#F00"> Veuillez cliquez sur le button ( Oui ) pour ajouter autre experiences!</h6>--><hr>');
            $("#next-action-please").val("Suivant");

            $('.datepicker').datepicker({
                dateFormat: 'yy-mm-dd'
            });
            
            $( function() {
                var dateFormat = "yy-mm-dd",
                  from = $( ".date_deb" )
                    .datepicker({
                      defaultDate: "+1w",
                      changeMonth: true,
                      numberOfMonths: 1
                    })
                    .on( "change", function() {
                      to.datepicker( "option", "minDate", getDate( this ) );
                      console.log($( ".date_deb" ).datepicker());
                    }),
                  to = $( ".date_f" ).datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1
                  })
                  .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                    console.log($( ".date_f" ).datepicker());
                  });
             
                function getDate( element ) {
                  var date;
                  try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                  } catch( error ) {
                    date = null;
                  }
             
                  return date;
                }
              } );
            
            if( count_exp ==1) {
                $("h3.sli1").click(function() {
                    $(".form-experience_1").slideToggle();

                });
                $(".closed_1").click(function() {
            
                    $(".form-experience_1").remove();
                    $(".sli1").remove();
                    count_exp=0;
                    dd=1;
                    coun1--;
                    coun--;
                });
                
                coun1++;
                coun++;
            }

            if( count_exp ==2) {
                
                $("h3.sli2").click(function() {
                   $(".form-experience_2").slideToggle();
                });

               
                $(".closed_2").click(function() {
                    
                    $(".form-experience_2").remove();
                    $(".sli2").remove();
                    dd=1;
                    count_exp=1;
                    coun2--;
                    coun--;
                    
                });
                
               coun2++;
               coun++;
            }
           
            if( count_exp ==3) {
                $("h3.sli3").click(function() {
                    $(".form-experience_3").slideToggle();
                });

                $(".closed_3").click(function() {
            
                    $(".form-experience_3").remove();
                    $(".sli3").remove();
                    dd=1;
                    count_exp=2;
                    coun3--;
                    coun--;
                });
             
                coun3++;
                coun++;
            }   

            if( count_exp ==4) {
                $("h3.sli4").click(function() {
                    $(".form-experience_4").slideToggle();
                });

                $(".closed_4").click(function() {
            
                    $(".form-experience_4").remove();
                    $(".sli4").remove();
                    dd=1;
                    count_exp=3;
                    coun4--;
                    coun--;
                });
           
                coun4++;
                coun++;
            } 

            if( count_exp ==5) {
                $("h3.sli5").click(function() {
                    $(".form-experience_5").slideToggle();
                });

                $(".closed_5").click(function() {
            
                    $(".form-experience_5").remove();
                    $(".sli5").remove();
                    dd=1;
                    count_exp=4;
                    coun5--;
                    coun--;
                });
         
                coun5++;
                coun++;
            } 

            if(dd==1 && coun1 >=1 && coun2 >=1 && coun3 >=1 && coun4 >=1 && coun5 >=1)
            {   
                count_exp=7;
            }
            else if(coun1 ==0)
            {
                count_exp=0;
            }
            else if(coun2 ==0)
            {
                count_exp=1;
            }
            else if(coun3 ==0)
            {
                count_exp=2;
            }
            else if(coun4 ==0)
            {
                count_exp=3;
            }
            else if(coun5 ==0)
            {
                count_exp=4;
            }

            $("#total_experiences").val(coun);

            var conHeight ;
            conHeight=$(".container-table-postuler").height();
            conHeight = conHeight + 520;
            $(".container-table-postuler").css('height' , conHeight  );
            $(this).val("Ajouter");
            if( count_exp ==2 || count_exp==3 ) {
                count_exp = count_exp -1;
                $(".form-experience_"+ count_exp).slideUp(1000);
                count_exp = count_exp+1;
            }
        }
    });

    var count_langues = 3;
    var count = 3;
    $("#add_langues").on("click", function() {
        if( count_langues >= 5 ) {
            
        } else {
            count_langues++;
            // $("#langues-content").append('<div class="form-langues_' + count_langues + '"><div class="form-row"><div class="form-group col-sm-6"><label for="langue_' + count_langues + '" >Langue : </label><b style="color:#F00"> *</b> <select class="form-control" id="langue_' + count_langues + '" name="langue_' + count_langues + '"><option disabled selected> -- </option><option>Arabe</option><option>Francais</option><option>Amazigh</option> <option>Anglais</option><option>Espagnol</option></select></div><div class="form-group col-sm-6"> <label for="niveau_' + count_langues + '" >Niveau : </label><b style="color:#F00"> *</b><select class="form-control" id="niveau_' + count_langues + '" name="niveau_' + count_langues + '"><option disabled selected> -- </option> <option>Notions</option><option>Intermediaire</option><option>Avancé</option><option>Excellent</option></select> </div>  </div> </div>');

            count++;
            var original = document.getElementById('form-langues_1');
            var clone = original.cloneNode(true); // "deep" clone
            clone.id = "form-langues_" + count;
            clone.childNodes[1].childNodes[1].childNodes[1].htmlFor = 'langue_' + count;
            clone.childNodes[1].childNodes[1].childNodes[4].setAttribute('name',  'langue_' + count);
            clone.childNodes[1].childNodes[3].childNodes[1].htmlFor = 'niveau_' + count;
            clone.childNodes[1].childNodes[3].childNodes[4].setAttribute('name',  'niveau_' + count);
            original.parentNode.appendChild(clone);

            $("#total_langues").val(count);
        }
    });

    $('#etablissement').on('change', function() {
        if( $("#etablissement").val() == 1 ) {
            $("#autre_etablissement").prop('disabled', false);
        } else {
            $("#autre_etablissement").prop("disabled", true);
        }
    });

    $('#specialite').on('change', function() {
        if( $(this).val() == 1 ) {
            $("#autre_specialite").prop('disabled', false);
        } else {
            $("#autre_specialite").prop("disabled", true);
        }
    });

    $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        yearRange: '1950:2050',
        changeYear: true
    });
    $(".datepicker").attr("autocomplete", "off");

    var selected = [];

    $("#offres_table")
        .on( 'order.dt',  function () {
            $('#offres_table #a_afficher').on('click', function () {

                $("#afficher_r_hidden_inputs").val( $(this).val() );
                var afficher_value = $(this).val();

                if ( !$(this).prop('checked') )
                {
                    $("#afficher_r_hidden_inputs").append(
                        $('<input>')
                           .attr('type', 'hidden')
                           .attr('name', 'afficher_r_hidden[]')
                           .attr('id', 'id_afficher_r_hidden')
                           .val($(this).val())
                    );

                    $('#afficher_hidden_inputs').children().each(function(i, obj) {
                        if( afficher_value == obj.value ) {
                            obj.remove();
                        }
                    });

                } else {

                    $("#afficher_hidden_inputs").append(
                        $('<input>')
                           .attr('type', 'hidden')
                           .attr('name', 'afficher_hidden[]')
                           .attr('id', 'id_afficher_hidden')
                           .val($(this).val())
                    );

                    $('#afficher_r_hidden_inputs').children().each(function(i, obj) {
                        if( afficher_value == obj.value ) {
                            obj.remove();
                        }
                    });
                }
            });
        })
        .on( 'search.dt', function () {
            $('#offres_table #a_afficher').on('click', function (){
                
                $("#afficher_r_hidden_inputs").val( $(this).val() );
                var afficher_value = $(this).val();

                if ( !$(this).prop('checked') )
                {
                    $("#afficher_r_hidden_inputs").append(
                        $('<input>')
                           .attr('type', 'hidden')
                           .attr('name', 'afficher_r_hidden[]')
                           .attr('id', 'id_afficher_r_hidden')
                           .val($(this).val())
                    );

                    $('#afficher_hidden_inputs').children().each(function(i, obj) {
                        if( afficher_value == obj.value ) {
                            obj.remove();
                        }
                    });

                } else {

                    $("#afficher_hidden_inputs").append(
                        $('<input>')
                           .attr('type', 'hidden')
                           .attr('name', 'afficher_hidden[]')
                           .attr('id', 'id_afficher_hidden')
                           .val($(this).val())
                    );

                    $('#afficher_r_hidden_inputs').children().each(function(i, obj) {
                        if( afficher_value == obj.value ) {
                            obj.remove();
                        }
                    });
                }
            });
        })
        .on( 'page.dt',   function () {
            $('#offres_table #a_afficher').on('click', function (){
                
                $("#afficher_r_hidden_inputs").val( $(this).val() );
                var afficher_value = $(this).val();

                if ( !$(this).prop('checked') )
                {
                    $("#afficher_r_hidden_inputs").append(
                        $('<input>')
                           .attr('type', 'hidden')
                           .attr('name', 'afficher_r_hidden[]')
                           .attr('id', 'id_afficher_r_hidden')
                           .val($(this).val())
                    );

                    $('#afficher_hidden_inputs').children().each(function(i, obj) {
                        if( afficher_value == obj.value ) {
                            obj.remove();
                        }
                    });

                } else {

                    $("#afficher_hidden_inputs").append(
                        $('<input>')
                           .attr('type', 'hidden')
                           .attr('name', 'afficher_hidden[]')
                           .attr('id', 'id_afficher_hidden')
                           .val($(this).val())
                    );

                    $('#afficher_r_hidden_inputs').children().each(function(i, obj) {
                        if( afficher_value == obj.value ) {
                            obj.remove();
                        }
                    });
                }
            });
        })
        .DataTable({
        "pageLength": 10,
        "order": [[ 1, 'desc' ]],
        "language": {
            processing:     "Traitement en cours...",
            search:         "Rechercher&nbsp;:",
            lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "Chargement en cours...",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable:     "Aucune donnée disponible dans le tableau",
            paginate: {
                first:      "Premier",
                previous:   "Pr&eacute;c&eacute;dent",
                next:       "Suivant",
                last:       "Dernier"
            },
            aria: {
                sortAscending:  ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    $("#offres_table2")
    .on( 'order.dt',  function () {
        $('#offres_table #a_afficher').on('click', function () {

            $("#afficher_r_hidden_inputs").val( $(this).val() );
            var afficher_value = $(this).val();

            if ( !$(this).prop('checked') )
            {
                $("#afficher_r_hidden_inputs").append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', 'afficher_r_hidden[]')
                       .attr('id', 'id_afficher_r_hidden')
                       .val($(this).val())
                );

                $('#afficher_hidden_inputs').children().each(function(i, obj) {
                    if( afficher_value == obj.value ) {
                        obj.remove();
                    }
                });

            } else {

                $("#afficher_hidden_inputs").append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', 'afficher_hidden[]')
                       .attr('id', 'id_afficher_hidden')
                       .val($(this).val())
                );

                $('#afficher_r_hidden_inputs').children().each(function(i, obj) {
                    if( afficher_value == obj.value ) {
                        obj.remove();
                    }
                });
            }
        });
    })
    .on( 'search.dt', function () {
        $('#offres_table #a_afficher').on('click', function (){
            
            $("#afficher_r_hidden_inputs").val( $(this).val() );
            var afficher_value = $(this).val();

            if ( !$(this).prop('checked') )
            {
                $("#afficher_r_hidden_inputs").append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', 'afficher_r_hidden[]')
                       .attr('id', 'id_afficher_r_hidden')
                       .val($(this).val())
                );

                $('#afficher_hidden_inputs').children().each(function(i, obj) {
                    if( afficher_value == obj.value ) {
                        obj.remove();
                    }
                });

            } else {

                $("#afficher_hidden_inputs").append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', 'afficher_hidden[]')
                       .attr('id', 'id_afficher_hidden')
                       .val($(this).val())
                );

                $('#afficher_r_hidden_inputs').children().each(function(i, obj) {
                    if( afficher_value == obj.value ) {
                        obj.remove();
                    }
                });
            }
        });
    })
    .on( 'page.dt',   function () {
        $('#offres_table #a_afficher').on('click', function (){
            
            $("#afficher_r_hidden_inputs").val( $(this).val() );
            var afficher_value = $(this).val();

            if ( !$(this).prop('checked') )
            {
                $("#afficher_r_hidden_inputs").append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', 'afficher_r_hidden[]')
                       .attr('id', 'id_afficher_r_hidden')
                       .val($(this).val())
                );

                $('#afficher_hidden_inputs').children().each(function(i, obj) {
                    if( afficher_value == obj.value ) {
                        obj.remove();
                    }
                });

            } else {

                $("#afficher_hidden_inputs").append(
                    $('<input>')
                       .attr('type', 'hidden')
                       .attr('name', 'afficher_hidden[]')
                       .attr('id', 'id_afficher_hidden')
                       .val($(this).val())
                );

                $('#afficher_r_hidden_inputs').children().each(function(i, obj) {
                    if( afficher_value == obj.value ) {
                        obj.remove();
                    }
                });
            }
        });
    })
    .DataTable({
    "pageLength": 10,
    "order": [[ 1, 'desc' ]],
    "language": {
        processing:     "Traitement en cours...",
        search:         "Rechercher&nbsp;:",
        lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
        info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
        infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
        infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        infoPostFix:    "",
        loadingRecords: "Chargement en cours...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "Aucune donnée disponible dans le tableau",
        paginate: {
            first:      "Premier",
            previous:   "Pr&eacute;c&eacute;dent",
            next:       "Suivant",
            last:       "Dernier"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
    }
});

    $("#save_afficher_offre").on("click", function() {
        $("#offres_table_filter input[type=search]").val("");
        $("#offres_table_filter input[type=search]").trigger('keyup');
    });

    $( function() {
        var dateFormat = "yy-mm-dd",
          from = $( ".date_deb" )
            .datepicker({
              defaultDate: "+1w",
              changeMonth: true,
              numberOfMonths: 1
            })
            .on( "change", function() {
              to.datepicker( "option", "minDate", getDate( this ) );
              console.log($( ".date_deb" ).datepicker());
            }),
          to = $( ".date_f" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1
          })
          .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
            console.log($( ".date_f" ).datepicker());
          });
     
        function getDate( element ) {
          var date;
          try {
            date = $.datepicker.parseDate( dateFormat, element.value );
          } catch( error ) {
            date = null;
          }
     
          return date;
        }
      } );
});