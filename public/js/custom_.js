$(document).ready(function () {

    var main_exps = ["1", "2", "3", "4", "5"];
    var count_exps = [];
    var deleted_number = [];

    function change_element_number(number) {
        $("h3.sli" + number).click(function() {
            $(".form-experience_" + number).slideToggle();
        });
        $("h3.sli" + number).css("padding", "6px");
        $("h3.sli" + number).css("border-top", "5px solid white");
    }
    
    var cpt = -1;

    $("#add_experience").on("click", function() {
        // console.log( "MAIN EXPS = " + main_exps );
        console.log( "COUNT EXPS = " + count_exps );
        console.log( "DELETED NUMBERS = " + deleted_number );
        console.log( "COUNTER = " + cpt);

        if( cpt == 4 ) {
        } else {
            cpt++;
            count_exp = main_exps[cpt];
            count_exps.push( main_exps[cpt] );
            main_exps.splice(main_exps[cpt], 0);
            $("#experience-content").append('<div class="main-form-experience_'+ count_exp+'"><div test="'+ count_exp+'" id="sli-title-'+ count_exp+'" class="clearfix exp_style"><h3 class="float-left exp_style sli'+ count_exp+' text-center"> Experience N°' + count_exp + '</h3><i class="float-right sli-icon fa fa-window-close"></i></div><div class="form-experience_' + count_exp + '"><div class=""><label for="poste_' + count_exp + '" >Poste occupe</label><b style="color:#F00"> *</b><input id="poste_' + count_exp + '" class="form-control" required="true"  type="text" name="poste_' + count_exp + '" /> </div> <div class=""><label for="salaire_' + count_exp + '">Salaire</label><b style="color:#F00"> *</b><input id="salaire_' + count_exp + '" class="form-control" required="true" name="salaire_' + count_exp + '"  /></div> <div class=""><label for="employeur_' + count_exp + '">Employeur</label><b style="color:#F00"> *</b><input id="employeur_' + count_exp + '" class="form-control" required="true" name="employeur_' + count_exp + '"  /></div> <div class=""><label for="secteur_' + count_exp + '">Secteur</label><b style="color:#F00"> *</b><input id="secteur_' + count_exp + '" class="form-control" size="60" maxlength="128" required="true" name="secteur_' + count_exp + '"  /></div><div class="form-row"><div class="form-group col-sm-6"><label for="date_debut_' + count_exp + '">Date Début</label><b style="color:#F00"> *</b> <input class="form-control"   required="true"  placeholder="Format JJ/MM/AAAA" id="date_debut_' + count_exp + '" name="date_debut_' + count_exp + '"  /></div> <div class="form-group col-sm-6"><label for="date_fin_' + count_exp + '">Date Fin</label><b style="color:#F00"> *</b> <input class="form-control"   required="true"  placeholder="Format JJ/MM/AAAA" id="date_fin_' + count_exp + '" name="date_fin_' + count_exp + '"  /></div> </div></div></div>');
            $("#next-action-please").val("Suivant");
            $("#total_experiences").val(count_exp);
            change_element_number(count_exp);

            // if( deleted_number.length == 0 ) {
            //     change_element_number(count_exp);
            // } else {
            //     change_element_number(deleted_number[0]);
            //     deleted_number.splice(deleted_number, 0);
            // }

            // if( deleted_number.length != 0 ) {
            //     count_added = deleted_number[0];
            //     count_exp ++;
            //     $("#experience-content").append('<div class="main-form-experience_'+ count_added+'"><div test="'+ count_added+'" id="sli-title-'+ count_added+'" class="clearfix exp_style"><h3 class="float-left exp_style sli'+ count_added+' text-center"> Experience N°' + count_added + '</h3><i class="float-right sli-icon fa fa-window-close"></i></div><div class="form-experience_' + count_added + '"><div class=""><label for="poste_' + count_added + '" >Poste occupe</label><b style="color:#F00"> *</b><input id="poste_' + count_added + '" class="form-control" required="true"  type="text" name="poste_' + count_added + '" /> </div> <div class=""><label for="salaire_' + count_added + '">Salaire</label><b style="color:#F00"> *</b><input id="salaire_' + count_added + '" class="form-control" required="true" name="salaire_' + count_added + '"  /></div> <div class=""><label for="employeur_' + count_added + '">Employeur</label><b style="color:#F00"> *</b><input id="employeur_' + count_added + '" class="form-control" required="true" name="employeur_' + count_added + '"  /></div> <div class=""><label for="secteur_' + count_added + '">Secteur</label><b style="color:#F00"> *</b><input id="secteur_' + count_added + '" class="form-control" size="60" maxlength="128" required="true" name="secteur_' + count_added + '"  /></div><div class="form-row"><div class="form-group col-sm-6"><label for="date_debut_' + count_added + '">Date Début</label><b style="color:#F00"> *</b> <input class="form-control"   required="true"  placeholder="Format JJ/MM/AAAA" id="date_debut_' + count_added + '" name="date_debut_' + count_added + '"  /></div> <div class="form-group col-sm-6"><label for="date_fin_' + count_added + '">Date Fin</label><b style="color:#F00"> *</b> <input class="form-control"   required="true"  placeholder="Format JJ/MM/AAAA" id="date_fin_' + count_added + '" name="date_fin_' + count_added + '"  /></div> </div></div></div>');
            //     $("#next-action-please").val("Suivant");
            //     $("#total_experiences").val(count_exp);
            //     change_element_number(count_added);
            //     deleted_number.splice( deleted_number[0], 0 );
            //     count_exps.push();
            // }
        }

        $(".sli-icon").css("font-size", "36px");
        $(".sli-icon").css("padding", "10px");

        $("#sli-title-" + count_exp).children(".sli-icon").click(function () {
            $(this).parent().parent().remove();
            count_exp = count_exp - 1;
            var texte = $(this).parent().parent("div").attr("class").replace('main-form-experience_', '');
            deleted_number.push( texte );
            count_exps.splice(count_exps.indexOf(texte), 1);
        });

        console.log("TOTAL = " + count_exp);

        // change height of width!
        // var conHeight ;
        // conHeight=$(".container-table-postuler").height();
        // conHeight = conHeight + 520;
        // $(".container-table-postuler").css('height' , conHeight);
        // $(this).val("Ajouter");
        // count_exp = count_exp - 1;
        // $(".form-experience_"+ count_exp).slideUp(1000);
        // count_exp = count_exp +1;
    });

    var count_langues = 1;
    var count = 1;
    $("#add_langues").on("click", function() {
        if( count_langues >= 3 ) {
            
        } else {
            count_langues++;
            // $("#langues-content").append('<div class="form-langues_' + count_langues + '"><div class="form-row"><div class="form-group col-sm-6"><label for="langue_' + count_langues + '" >Langue : </label><b style="color:#F00"> *</b> <select class="form-control" id="langue_' + count_langues + '" name="langue_' + count_langues + '"><option disabled selected> -- </option><option>Arabe</option><option>Francais</option><option>Amazigh</option> <option>Anglais</option><option>Espagnol</option></select></div><div class="form-group col-sm-6"> <label for="niveau_' + count_langues + '" >Niveau : </label><b style="color:#F00"> *</b><select class="form-control" id="niveau_' + count_langues + '" name="niveau_' + count_langues + '"><option disabled selected> -- </option> <option>Notions</option><option>Intermediaire</option><option>Avancé</option><option>Excellent</option></select> </div>  </div> </div>');

            count++;
            var original = document.getElementById('form-langues_1');
            var clone = original.cloneNode(true); // "deep" clone
            clone.id = "form-langues_" + count;
            clone.childNodes[1].childNodes[1].childNodes[1].htmlFor = 'langue_' + count;
            clone.childNodes[1].childNodes[1].childNodes[4].setAttribute('name',  'langue_' + count);
            clone.childNodes[1].childNodes[3].childNodes[1].htmlFor = 'niveau_' + count;
            clone.childNodes[1].childNodes[3].childNodes[4].setAttribute('name',  'niveau_' + count);
            original.parentNode.appendChild(clone);

            $("#total_langues").val(count);
        }
    });

    $('#etablissement').on('change', function() {
        if( $("#etablissement").val() == 1 ) {
            $("#autre_etablissement").prop('disabled', false);
        } else {
            $("#autre_etablissement").prop("disabled", true);
        }
    });

    $('#specialite').on('change', function() {
        if( $(this).val() == 1 ) {
            $("#autre_specialite").prop('disabled', false);
        } else {
            $("#autre_specialite").prop("disabled", true);
        }
    });

});