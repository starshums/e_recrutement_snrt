@extends('layouts.app')

@section('content')
<div class="container">
    <div class="container-header clearfix">
        <h2> <i class="fa fa-home"> </i> LISTE DES OPERATIONS DE RECRUTEMENT </h2>
    </div>

    <div class="container-menu">
    </div>

    @if( $offres->count() != 0 )
    <div class="container-table">

    <div class="container clearfix">
        <h3 class="float-left"> <i> <i class="fa fa-list">    </i> LISTES DES OFFRES DISPONIBLE CE MOMENT </i></h3>
        @if (Auth::user()->hasRole("ADMIN") == 1)
        <a href="offres/afficher" class=" ml-2 float-right"> <strong> <i class="fa fa-edit"> </i>
                Modifier Affichage</strong> </a>
        @endif
    </div>
    <hr>

        <table id="offres_table" class="table">
            <thead>
                <th>Code Operration</th>
                <th>Métier</th>
                <th>Operation</th>
            </thead>
            <tbody>
                @foreach ( $offres as $offre )
                <tr>
                    <td width="300" style="font-size:12,5px" scope="col" class="">

                    <strong> {{ $offre->code_operation }} </strong>

                    </td>
                    <td width="300" style="font-size:12,5px" scope="col" class="">

                        <a href="offres/details/{{ $offre->id_offre }}" style="color: #191c40;">
                        <strong> <i class="fa fa-file"> </i> {{ $offre->metier }} </strong>
                        </a>

                    </td>
                    <td width="300" style="font-size:12,5px" scope="col" class="">

                    <strong> {{ $offre->operation }} </strong>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="main-pagination-div"> </div>


    </div>
    @else
    <div class="container-table">
        <h4> <i class="fa fa-file"> </i> rien à afficher ...</h4>
        <div class="container">
            @if (Auth::user()->hasRole("ADMIN") == 1)
            <a href="offres/afficher" class="ml-2 float-right"> <strong> <i class="fa fa-edit"> </i>
                    Modifier Affichage </strong> </a>
            @endif
        </div>
    </div>
    @endif

    @if( $offres_r->count() != 0 )
    <div class="container-table">

    <div class="container clearfix">
        <h3 class="float-left"> <i> <i class="fa fa-list">    </i> LISTES DES RÉSULTATS </i> </h3>
        @if (Auth::user()->hasRole("ADMIN") == 1)
        <a href="offres/afficher/r" class=" ml-2 float-right"> <strong> <i class="fa fa-edit"> </i>
                Modifier Affichage </strong> </a>
        @endif
    </div>
    <hr>

        <table id="offres_table2" class="table">
            <thead>
                <th>Code Operration</th>
                <th>Métier</th>
                <th>Operation</th>
            </thead>
            <tbody>
                @foreach ( $offres_r as $offre )
                <tr>
                    <td width="300" style="font-size:12,5px" scope="col" class="">

                    <strong> {{ $offre->code_operation }} </strong>

                    </td>
                    <td width="300" style="font-size:12,5px" scope="col" class="">

                        <a href="offres/details/{{ $offre->id_offre }}" style="color: #191c40;">
                        <strong> <i class="fa fa-file"> </i> {{ $offre->metier }} </strong>
                        </a>

                    </td>
                    <td width="300" style="font-size:12,5px" scope="col" class="">

                    <strong> {{ $offre->operation }} </strong>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="main-pagination-div"> </div>


    </div>
    @else
    <div class="container-table">
        <h4> <i class="fa fa-file"> </i> rien à afficher ...</h4>
        <div class="container">
            @if (Auth::user()->hasRole("ADMIN") == 1)
            <a href="offres/afficher/r" class="ml-2 float-right"> <strong> <i class="fa fa-edit"> </i>
                    Modifier Affichage </strong> </a>
            @endif
        </div>
    </div>
    @endif

</div>
@endsection