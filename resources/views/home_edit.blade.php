@extends('layouts.app')

@section('content')
<div class="container">
                <div class="container-header clearfix">
                    <h2> <i class="fa fa-home">         </i>     Homepage  </h2>
                </div>
                
                @if( $offres->count() != 0 )
                <div class="container-table">

                <form method="post" action="{{ route('offres.modifier_afficher_homepage') }}">
                        {{ csrf_field() }}

                    <table id="offres_table" class="table-hover text-center">
                            <thead>
                                <tr>
                                    <th>Cocher</th>
                                    <th width="" style="font-size:12,5px" scope="col">Code Operation</th>
                                    <th width="" style="font-size:12,5px" scope="col">Operation</th>
                                    <th width="" style="font-size:12,5px" scope="col">Métier</th>
                                    <th width="" style="font-size:12,5px" scope="col">Date Publication</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $offres as $offre )
                                    <tr>
                                        <td><input type="checkbox" value="{{ $offre->id_offre }}" id="a_afficher" name="a_afficher[]" class="form-control" ></td>
                                        <td width="160" style="font-size:12,5px" scope="col">{{ $offre->code_operation }}</td>
                                        <td width="100" style="font-size:12,5px" scope="col">{{ $offre->operation }}</td>
                                        <td width="260" style="font-size:12,5px" scope="col">{{ $offre->metier }}</td>
                                        <td width="200" style="font-size:12,5px" scope="col">{{ $offre->date_offre }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                    </table>

                    <div class="container col-sm-4 text-center">
                            <button type="submit" class="btn-container"> <i class="fa fa-save">     </i> Enregistrer Selection(s)</button>
                        </div>
                        
                    </form>

                    <hr>
                    <div class="main-pagination-div"> </div>
                    
                
                </div>
                @else
                    <div class="container-table">
                        <h4> <i class="fa fa-file">  </i> rien à afficher ...</h4>
                    </div>
                @endif

            </div>
@endsection