<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNRT | E-RECRUTEMENT</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    @if (Auth::guest())
        <link rel="stylesheet" href="{{ asset('css/mainUser.css') }}">
    @endif
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
</head>
<body>

    <div class="wrapper">

        <div class="main-container" style="margin-top: 4%;">
            <div class="container">
                <div class="container-header clearfix">
                </div>
                <div class="container-table fadeInDown">
                        <div id="formContent">
                                <!-- Tabs Titles -->
                                <div class="logo">
                                        <a href="{{ url('/') }}">
                                            <img id="snrt_logo" src="../img/snrt_ins_logo.png" alt="SNRT LOGO" height="100px" >
                                        </a>
                                    </div>
                            
                                <!-- Icon -->
                                <div class="fadeIn first">
                                    <h1 class="text-center">e-Recrutement</h1>
                                  <h6 class="text-center">  Veuillez entrez votre adresse email </h6>
                                </div>
                                <hr>

                                <form class="fadeIn first" method="POST" action="{{ route('password.email') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group ">
                                        <label for="email" class=" control-label">E-Mail Address</label>
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                          <small id="emailHelp" class="form-text text-muted">Nous ne partagerons jamais vos données avec qui que ce soit.</small>
                                    </div>

                                    <div class="form-group">
                                        <div class="container text-center">
                                            <button type="submit" class="btn-container">
                                                <i class="fa fa-sign-in-alt">     </i> Envoyer le lien de mot de passe réinitialisé
                                            </button>
                                        </div>
                                    </div>
                                </form> <br>
                            
                              </div>
                      
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>
