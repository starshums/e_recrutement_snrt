<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNRT | E-RECRUTEMENT</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    @if (Auth::guest())
        <link rel="stylesheet" href="{{ asset('css/mainUser.css') }}">
    @endif
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
</head>
<body>

    <div class="wrapper">

        <div class="main-container" style="margin: 4% 0;">
            <div class="container">
                <div class="container-header clearfix">
                </div>
                <div class="container-table fadeInDown">
                        <div id="formContent">
                                <!-- Tabs Titles -->
                                <div class="logo">
                                        <a href="{{ url('/') }}">
                                            <img id="snrt_logo" src="img/snrt_ins_logo.png" alt="SNRT LOGO" height="100px" >
                                        </a>
                                    </div>
                            
                                <!-- Icon -->
                                <div class="fadeIn first">
                                    <h1 class="text-center">e-Recrutement</h1>
                                    <h6 class="text-center">  Ces informations vous permettront d'accéder à votre compte par la suite</h6>
                                    <br>
                                    <h6 style="color: red;">
                                        <strong>
                                            (*) : Champ Obligatoire
                                        </strong>
                                    </h6>
                                    <h6 style="color: red;">
                                        <strong>
                                            (**) : Les informations concernant votre profil seront envoyés à votre adresse mail
                                        </strong>
                                    </h6>
                                </div>
                                <hr>
                            
                                <!-- Login Form -->
                                <form class="fadeIn first" method="POST" action="{{ route('register') }}">
                                    {{ csrf_field() }}
                                        <div class="form-row">

                                            <div class="col-md-3 {{ $errors->has('cin') ? ' has-error' : '' }}">
                                                <label for="cin" class="control-label">CIN</label>
                                                <input id="cin" type="text" class="form-control" name="cin" value="{{ old('cin') }}" required autofocus>

                                                @if ($errors->has('cin'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('cin') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Exemple AB9999 (*)</small>
                                            </div>

                                            <div class="col-md-5 {{ $errors->has('nom') ? ' has-error' : '' }}">
                                                <label for="nom" class="control-label">Votre Nom</label>
                                                <input id="nom" type="text" class="form-control" name="nom" value="{{ old('nom') }}" required autofocus>

                                                @if ($errors->has('nom'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('nom') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Nom en majuscule (*)</small>
                                            </div>

                                            <div class="col-md-4 {{ $errors->has('prenom') ? ' has-error' : '' }}">
                                                <label for="prenom" class="control-label">Votre Prénom</label>
                                                <input id="prenom" type="text" class="form-control" name="prenom" value="{{ old('prenom') }}" required autofocus>

                                                @if ($errors->has('prenom'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('prenom') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Prénom en majuscule (*)</small>
                                            </div>

                                        </div>

                                        <br>
                                        <div class="form-row">
                                            
                                            <div class="col-md-6 {{ $errors->has('date_naissance') ? ' has-error' : '' }}">
                                                <label for="date_naissance" class="control-label">Entez votre Date de Naissance</label>
                                                <input id="date_naissance" type="text" class="form-control datepicker" data-provide="datepicker" name="date_naissance" value="{{ old('date_naissance') }}" required autofocus>

                                                @if ($errors->has('date_naissance'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('date_naissance') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Date de Naissane (AAAA/MM/JJ) (*)</small>
                                            </div>

                                            <div class="col-md-6 {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                                <label for="telephone" class="control-label">Téléphone</label>
                                                <input id="telephone" type="text" class="form-control" name="telephone" value="{{ old('telephone') }}" required autofocus>

                                                @if ($errors->has('telephone'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('telephone') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Téléphone (*)</small>
                                            </div>

                                        </div>

                                        <br>
                                        <div class="form-row">

                                            <div class="col-md-9 {{ $errors->has('adresse') ? ' has-error' : '' }}">
                                                <label for="adresse" class="control-label">Adresse</label>
                                                <input id="adresse" type="text" class="form-control" name="adresse" value="{{ old('adresse') }}" required autofocus>

                                                @if ($errors->has('adresse'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('adresse') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Adresse (*)</small>
                                            </div>

                                            <div class="col-md-3 {{ $errors->has('ville') ? ' has-error' : '' }}">
                                                <label for="ville" class="control-label">Ville</label>
                                                <input id="ville" type="text" class="form-control" name="ville" value="{{ old('ville') }}" required autofocus>

                                                @if ($errors->has('ville'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('ville') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Ville (*)</small>
                                            </div>

                                        </div>
                                        
                                        <br>
                                        <div class="form-row">

                                            <div class="col-md-4 {{ $errors->has('pays') ? ' has-error' : '' }}">
                                                <label for="pays" class="control-label">Pays</label>
                                                <input id="pays" type="text" class="form-control" name="pays" value="{{ old('pays') }}" required autofocus>

                                                @if ($errors->has('pays'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('pays') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Pays (*)</small>
                                            </div>

                                            <div class="col-md-8 {{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="control-label">Adress e-mail</label>
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Adresse e-mail (*) (**)</small>
                                            </div>

                                        </div>

                                        <br>
                                        <div class="form-row ">

                                            
                                            <div class="col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password" class="control-label">Mot de passe</label>
                                                <input id="password" type="password" class="form-control" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Mot de passe (*)</small>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <label for="password-confirm" class="control-label">Confirmation de mot de passe</label>
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                <small id="" class="form-text text-muted" style="color: red;">Confirmation de mot de passe (*)</small>
                                            </div>

                                        </div>

                                        <br>
                                        <div>
                                            <!-- {!! NoCaptcha::renderJs() !!}
                                            {!! NoCaptcha::display() !!}

                                            @if ($errors->has('g-recaptcha-response'))
                                                <span class="help-block" style="color: red !important;">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                            @endif -->
                                        </div>
                                        <br>
                                        <button type="submit" class="btn-container"> <i class="fa fa-sign-in-alt">     </i> S'inscrire</button>
                                      </form>
                            
                                      <br>
                                <!-- Remind Passowrd -->
                                <div id="formFooter">
                                  <a class="underlineHover" href="login">Se connecter!</a>
                                </div>
                            
                              </div>
                      
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
    <script src="{{ asset('js/cc.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>