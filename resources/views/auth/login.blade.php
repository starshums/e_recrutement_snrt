<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SNRT | E-RECRUTEMENT</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    @if (Auth::guest())
        <link rel="stylesheet" href="{{ asset('css/mainUser.css') }}">
    @endif
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
</head>
<body>

    <div class="wrapper">

        <div class="main-container" style="margin-top: 4%;">
            <div class="container">
                <div class="container-header clearfix">
                </div>
                <div class="container-table fadeInDown">
                        <div id="formContent">
                                <!-- Tabs Titles -->
                                <div class="logo">
                                        <a href="{{ url('/') }}">
                                            <img id="snrt_logo" src="img/snrt_ins_logo.png" alt="SNRT LOGO" height="100px" >
                                        </a>
                                    </div>
                            
                                <!-- Icon -->
                                <div class="fadeIn first">
                                    <h1 class="text-center">e-Recrutement</h1>
                                  <h6>  Veillez se connecter pour ouvrire une session</h6>
                                </div>
                                <hr>
                            
                                <!-- Login Form -->
                                <br>
                                <form class="fadeIn first" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                        <div class="form-group ">
                                          <label for="cin">Carte d'Identité National (CIN)</label>
                                          <input id="cin" class="form-control" name="cin" value="{{ old('cin') }}" required autofocus>
                                            @if ($errors->has('cin'))
                                                <span class="help-block" style="color: red !important;">
                                                    <strong>{{ $errors->first('cin') }}</strong>
                                                </span>
                                            @endif
                                          <small id="emailHelp" class="form-text text-muted">We'll never share your data with anyone else.</small>
                                        </div>
                                        <div class="form-group">
                                          <label for="password">Mot de passe</label>
                                          <input id="password" type="password" class="form-control" name="password" required>
                                            @if ($errors->has('password'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <br>
                                        <div class="">
                                            {!! NoCaptcha::renderJs() !!}
                                            {!! NoCaptcha::display() !!}
                                            
                                            @if ($errors->has('g-recaptcha-response'))
                                                    <span class="help-block" style="color: red !important;">
                                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                    </span>
                                                @endif
                                        </div>
                                        <br>
                                        <button type="submit" class="btn-container"> <i class="fa fa-sign-in-alt">     </i> Se Connecter</button>
                                      </form>
                            
                                      <br>
                                <!-- Remind Passowrd -->
                                <div id="formFooter">
                                  <a class="underlineHover" href="register">Nouvelle inscription!</a>
                                 
                                  <a class="underlineHover" href="{{ route('password.request') }}">
                                        Mot de passe oublié ?
                                    </a>
                                </div>
                            
                              </div>
                      
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>