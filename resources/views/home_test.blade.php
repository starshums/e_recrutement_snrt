<!DOCTYPE HTML>

<html>

<head>
	<title>E-recrutement-SNRT-</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="SNRT " />
	<meta name="keywords" content="jquery, form, sliding, usability, css3, validation, javascript" />
	<link rel="stylesheet" href="{{ asset('css/all.css') }}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" media="screen" />
	<link rel="stylesheet" href="{{asset('css/menu.css')}}" type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">


	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="{{asset('js/sliding.form.js')}}"></script>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
	<link rel="stylesheet" href="{{asset('css/main1.css')}}" />
	<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->

</head>

<body>

	<!-- Header -->
	<section id="header">
		<header>
			<img src="{{asset('img/snrtar.png')}}" width="280" height="199" />
			<h1>E-Recrutement</h1>
			<p>Bienvenue sur l'espace recrutement de la SNRT</p>
		</header>
		<footer>
			<a href="#banner" class="button style2 scrolly-middle">Les dernières offres publiées </a>


			<a href="#RESULTAT" class="button style2 scrolly-middle"> &nbsp;&nbsp;&nbsp;&nbsp; Résultats des opérations </a>
		</footer>
	</section>

	<!-- Banner -->
	<section id="banner">
		<div align="center"> <a href="#header"> <img src="{{asset('img/fleche.png')}}" width="79" height="67" /></a></div>
		<header>

			<h2>Liste des operations de recrutement</h2>
		</header>
		<footer>



			<div align="center">
				<TABLE width="927" style="border-collapse: collapse;width:1100px">

					@foreach ( $offres as $offre )
					<tr style="border: 1px solid black;">
						<th width="441" ROWSPAN=1 style="border: 1px solid black;">
							Opération n° {{ $offre->code_operation }}
							<b style="font-weight:900;color:#FC0;font-style:oblique"> </b> </th>
						<td width="447" style="border: 1px solid black;"><a href="../storage/app/OFFRES/{{$offre->id_offre}}/publication/{{$offre->fichier_offre}}" target="_blank">
								&nbsp;&nbsp;&nbsp;{{ $offre->metier }}
							</a></td>
					</tr>
					@endforeach


				</TABLE>
			</div>

			<br>
			<div class="12u$">
				<ul class="actions">
					<li><a href="#inscrip"><input type="submit" value="Inscription" /></a></li>
					<li><a href="#condidat"> <input type="submit" value="Postuler" /></a></li>
				</ul>
			</div>
		</footer>
	</section>
	<!-- Feature 1 -->
	<!-- Contact -->
	<article id="inscrip" class="container box style3">
		<div align="center"> <a href="#header"> <img src="{{asset('img/fleche.png')}}" width="79" height="67" /></a></div>
		<header>
			<h2> Inscription </h2><br>
		</header>

			<div class="12u$" align="center">
				<ul class="actions">
					<a href="#FAQ" class="button style1 scrolly-middle"> FAQ </a>
					<li><a  href="{{ url('/register') }}" class="button style1 scrolly-middle" value="Connexion"> Inscription </a></li>
				</ul>
			</div>
			</div>
		</form>
	</article>
	<article id="condidat" class="container box style3">
		<div align="center"> <a href="#header"> <img src="{{asset('img/fleche.png')}}" width="79" height="67" /></a></div>
		<header>
			<h2>Espace candidat</h2>
			<p>Connectez vous à l'espace candidat pour postuler</p>
		</header>
			<div class="row 50%">

				<div class="12u$">
					<ul class="actions" align="center">
						<li><a  href="{{ url('/login') }}" class="button style1 scrolly-middle" value="Connexion"> Connexion </a></li>
					</ul>
				</div>
			</div>
		</form>
	</article>
	<article id="FAQ" class="container box style3">
		<div align="center"> <a href="#header"> <img src="{{asset('img/fleche.png')}}" width="79" height="67" /></a></div>
		<header>
			<h2>Foire Aux Questions </h2>
			<p></p>
		</header>
		<p>

			<li> <B>Comment s’inscrire sur http://recrutement.snrt.ma/ ?</B></li>

			<FONT COLOR="#62AC1E"><B>En vous inscrivant sur http://recrutement.snrt.ma/, un message de confirmation sera
					envoyé à votre adresse email, si vous ne recevez pas le mail de confirmation juste après votre inscription,
					veuillez vérifier dans votre boîte SPAM.
				</B></FONT>
		</p>
		<p>
			<li> <B>Quels types de fichiers sont supportés sur http://recrutement.snrt.ma/</B></li>

			<FONT COLOR="#62AC1E"><B> Les types de fichier autorisés dans http://recrutement.snrt.ma/ sont limités aux
					fichiers PDF</B></FONT>
		</p>

		<p>
			<li> <B>Pourquoi un message d'erreur s'affiche-t-il lorsque je valide mon formulaire d'inscription ou de
					candidature?</B></li>

			<FONT COLOR="#62AC1E"><B> Pour contourner ce problème,vider le cache du navigateur et n'utilisez pas de caractères
					spéciaux tels que : ["],['],[()],[,],[;],[é],[à],[N°],...</B></FONT>
		</p>
		<p>
			<li> <B> Les conditions générales d'utilisation </B></li>

			<FONT COLOR="#62AC1E"><B> <a href="CNDP.pdf" TARGET="_blank">Télécharger</a> </br> </B></FONT>
		</p>
	</article>
	<article id="RESULTAT" class="container box style3">
		<div align="center"> <a href="#header"> <img src="{{asset('img/fleche.png')}}" width="79" height="67" /></a></div>
		<header>
			<h2>Résultats </h2>
			<p></p>
		</header>

			@foreach ( $offres_r as $offre )
			
			<span class="offers_list_home" >
				<li style="list-style: none !important;"><b>  Opération: {{ $offre->code_operation }} // Poste : {{ $offre->metier }}</b></li>
					@foreach ( $offre->offres_resultats as $fichier_resultat )
						<font color="#62AC1E">
						
						<table>
							<tr>
								<td><b>  {{ $fichier_resultat->description }}
								
							</b><br></td>
								<td align="right"><b> <a href="../storage/app/OFFRES/{{$offre->id_offre}}/resultat/{{$fichier_resultat->fichier_resultat}}"
								target="_blank" data-toggle="tooltip" title="Télécharger la {{ $fichier_resultat->description }}">
								 <i class="fa fa-cloud-download-alt" style="font-size: 16px;">     </i>Télécharger</a> </b><br></td>
							</tr>
						</table>

						</font>
					@endforeach
			</span>
			
			@endforeach

	</article>
	<section>
		<ul>






		</ul>
		<div>
			<ul class="menu">
				<li></li>
			</ul>
		</div>
	</section>

	<style>

		.offers_list_home a:hover {
			color: green !important;
		}

	</style>

	<!-- Scripts -->
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<script src="{{asset('js/jquery.scrolly.min.js')}}"></script>
	<script src="{{asset('js/jquery.poptrox.min.js')}}"></script>
	<script src="{{asset('js/skel.min.js')}}"></script>
	<script src="{{asset('js/util.js')}}"></script>
	<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
	<script src="{{asset('js/main.js')}}"></script>
	<script>
		window.onload = function () {
			var myInput = document.getElementById('myInput');
			myInput.onpaste = function (e) {
				e.preventDefault();
			}
		}
	</script>

	<script>
		window.onload = function () {
			var myInput = document.getElementById('motpasse');
			myInput.onpaste = function (e) {
				e.preventDefault();
			}
		}
	</script>

</body>

</html>