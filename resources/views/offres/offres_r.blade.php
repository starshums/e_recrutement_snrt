@extends('layouts.app')

@section('content')
<div class="container">
    <div class="container-header clearfix">
        <h2> <i class="fa fa-home"> </i> LISTE DES OPERATIONS DE RECRUTEMENT </h2>
    </div>

    <div class="container-menu">
        @if (Auth::user()->hasRole("ADMIN") == 1)
        <a href="../../offres/ajouter" class="container-details-link c-d-l-2 mr-2"> <strong> <i class="fa fa-save"> </i>
                Ajouter une nouvelle offre</strong> </a>
        <a href="../../offres/r" class="container-details-link c-d-l-2"> <strong> <i class="fa fa-list"> </i>
                Listes des Offres (Résultats)</strong> </a>
        @endif
    </div>

    @if( $offres_r->count() != 0 )
    <div class="container-table">

        <div class="container clearfix">
            <h3 class="float-left"> <i> <i class="fa fa-list"> </i> LISTES DES RÉSULTATS </i> </h3>
            @if (Auth::user()->hasRole("ADMIN") == 1)
            <a href="../../offres/afficher/r" class=" ml-2 float-right"> <strong> <i class="fa fa-edit"> </i>
                    Modifier Affichage</strong> </a>
            @endif
        </div>
        <hr>

        <table id="offres_table2" class="table">
            <thead>
                <th>Code Operration</th>
                <th>Métier</th>
                <th>Date Publication</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ( $offres_r as $offre )
                <tr>
                    <td width="300" style="font-size:12,5px" scope="col" class="">

                        <a href="../../offres/details/{{ $offre->id_offre }}" style="color: #191c40;">
                            <strong> {{ $offre->code_operation }} </strong>
                        </a>

                    </td>
                    <td width="300" style="font-size:12,5px" scope="col" class="">

                        <a href="../../offres/details/{{ $offre->id_offre }}" style="color: #191c40;">
                            <strong> <i class="fa fa-file"> </i> {{ $offre->metier }} </strong>
                        </a>

                    </td>
                    <td width="200" style="font-size:12,5px" scope="col">

                        <a href="../../offres/details/{{ $offre->id_offre }}" style="color: #191c40;">
                            <strong> {{ $offre->date_offre }} </strong>
                        </a>
                    </td>
                    <td width="150" style="font-size:12,5px" scope="col" class="text-center">

                        <div class="">
                        
                            <a data-toggle="tooltip" title="Plus de détail" class="container-details-link mr-1"
                                href="../../offres/details/{{ $offre->id_offre }}">
                                <i class="fa fa-list"> </i>
                            </a>

                            @if ( !Auth::user()->profile->suivi_offres->find($offre->id_offre) )
                                <a data-toggle="tooltip" title="Postuler à cette offre" class="container-details-link mr-1 container-details-link-orange"
                                    href="../../offres/postuler/{{ $offre->id_offre }}">
                                    <i class="fa fa-file"> </i> </a>
                            @else
                                <a data-toggle="tooltip" title="Télécharger le reçu" target="_blank" class="container-details-link-blue mr-1"
                                    href="../../telecharger/recu/{{ $offre->id_offre }}/{{ Auth::user()->profile->id_profil }}">
                                    <i class="fa fa-save"> </i>
                                </a>
                            @endif

                            <a data-toggle="tooltip" title="Télécharger l'offre" target="_blank" class="container-details-link container-details-link-green mr-1"
                                href="../../../storage/app/OFFRES/{{$offre->id_offre}}/publication/{{$offre->fichier_offre}}">
                                <i class="fa fa-cloud-download-alt"></i>
                            </a>

                        </div>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="main-pagination-div"> </div>


    </div>
    @else
    <div class="container-table">
        <h4> <i class="fa fa-file"> </i> rien à afficher ...</h4>
        <div class="container">
            @if (Auth::user()->hasRole("ADMIN") == 1)
            <a href="../../offres/afficher/r" class="ml-2 float-right"> <strong> <i class="fa fa-edit"> </i>
                    Modifier Affichage </strong> </a>
            @endif
        </div>
    </div>
    @endif

</div>
@endsection