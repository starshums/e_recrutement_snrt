@extends('layouts.app')

@section('content')
<div class="container">
                
                <div class="container-header clearfix">
                    <h2> <i class="fa fa-layer-group">         </i>     Liste des operations de recrutement  </h2>
                </div>

                <div class="container-menu">
                    @if (Auth::user()->hasRole("ADMIN") == 1)
                        <a href="../offres/ajouter" class="container-details-link c-d-l-2"> <strong> <i class="fa fa-save">         </i> Ajouter une nouvelle offre</strong> </a>
                    @endif
                </div>
                
                @if( $offres->count() != 0 )
                <div class="container-table">

                    <form method="post" action="{{ route('offres.afficher_homepage') }}">
                        {{ csrf_field() }}

                    <table id="offres_table" class="table-hover exp text-center">
                            <thead>
                                <tr>
                                    <th>Cocher</th>
                                    <th width="" style="font-size:12,5px" scope="col">Code Operation</th>
                                    <th width="" style="font-size:12,5px" scope="col">Operation</th>
                                    <th width="" style="font-size:12,5px" scope="col">Métier</th>
                                    <th width="" style="font-size:12,5px" scope="col">Date Publication</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ( $offres as $offre )
                                    <tr>
                                        @if ( $offre->a_afficher == "1" )
                                        <td>
                                            <input type="checkbox" value="{{ $offre->id_offre }}" id="a_afficher" name="a_afficher[]" class="form-control afficher_checked" checked>
                                            <!-- <a href="{{ url('/offres/uncheck', $offre->id_offre) }}" class="btn btn-danger"> - </a> -->
                                        </td>
                                        @else
                                            <td id="afficher_check">
                                                <input type="checkbox" value="{{ $offre->id_offre }}" id="a_afficher" name="a_afficher[]" class="form-control" >
                                            </td>
                                        @endif
                                        <td width="160" style="font-size:12,5px" scope="col">{{ $offre->code_operation }}</td>
                                        <td width="100" style="font-size:12,5px" scope="col">{{ $offre->operation }}</td>
                                        <td width="260" style="font-size:12,5px" scope="col">{{ $offre->metier }}</td>
                                        <td width="200" style="font-size:12,5px" scope="col">{{ $offre->date_offre }}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                    </table>

                        <div class="container col-sm-4 text-center">
                            <button type="submit" class="btn-container enr" id="save_afficher_offre"> <i class="fa fa-save">     </i> Enregistrer Selection(s)</button>
                        </div>

                        <div id="afficher_hidden_inputs">                      
                        </div>
                        <div id="afficher_r_hidden_inputs">                      
                        </div>
                        
                    </form>

                    <hr>
                    <div class="main-pagination-div"> </div>
                    
                
                </div>
                @else
                    <div class="container-table">
                        <h4> <i class="fa fa-file">  </i> rien à afficher ...</h4>
                    </div>
                @endif
            </div>
@endsection