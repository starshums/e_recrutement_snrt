@extends('layouts.app')

@section('content')
<div class="container">
                <div class="container-header clearfix">
                    <h2> <i class="fa fa-file">         </i>    Ajouter une nouvelle Offre  </h2>
                </div>
                <div class="container-table">
                <div id="formContent">
                            
                                <!-- Login Form -->
                                <h3 style="color: #444444;"><i class="fa fa-file">         </i>  Nouvelle Offre</h3>
                                <hr>
                                <form class="fadeIn first" method="POST" action="{{ route('offres.ajouter') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                          <label for="operation">Operation</label>
                                          <input id="operation" class="form-control" name="operation" value="{{ old('operation') }}" required autofocus>
                                        </div>

                                        <div class="form-group col-md-6">
                                          <label for="code_operation">Code Operation</label>
                                          <input id="code_operation" type="code_operation" class="form-control" name="code_operation" required>
                                        </div>
                                        <br>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                          <label for="metier">Métier</label>
                                          <input id="metier" class="form-control" name="metier" value="{{ old('metier') }}" required autofocus>
                                        </div>
                                        <br>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                          <label for="nbr_postes">Nombre de Postes</label>
                                          <input id="nbr_postes" class="form-control" name="nbr_postes" value="{{ old('nbr_postes') }}" required autofocus>
                                        </div>

                                        <div class="form-group col-md-4">
                                          <label for="date_offre">Date de Publication</label>
                                          <input id="date_offre"  class="form-control datepicker date_deb" data-date-format="yyyy-mm-dd"  name="date_offre" required>
                                        </div>
                                        
                                        <div class="form-group col-md-4">
                                          <label for="date_delai">Date Delai</label>
                                          <input id="date_delai"  class="form-control datepicker date_f" data-date-format="yyyy-mm-dd" name="date_delai" required>
                                        </div>
                                        <br>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                          <label for="file_offre">Fichier de publication</label>
                                          <input type="file" name="file_offre" id="file_offre" class="form-control">
                                        </div>
                                    </div>

                                    <br>
                                        <button type="submit" class="btn-container aj"> <i class="fa fa-save">     </i> Ajouter</button>
                                      </form>
                            
                                      <br>
                            
                              </div>
                </div>
            </div>
@endsection




