@extends('layouts.app')

@section('content')
<div class="container">

                <div class="container-header clearfix">
                    <h2> <i class="fa fa-file-alt">         </i>    Postuler à l' Offre N° {{ $offre->id_offre }} // Code Operation N° {{ $offre->code_operation }}   </h2>
                </div>

                <div class="container-table-postuler">
                    <h3 style="color: #444444;"><i class="fa fa-info-circle">         </i>  Postuler à cette offre </h3>
                    <hr>


<form class="steps" accept-charset="UTF-8" enctype="multipart/form-data" novalidate="" method="POST" action="{{ route('offres.postuler', $offre->id_offre) }}">
      {{ csrf_field() }}
        <ul id="progressbar" style="margin-left: 30px;">
          <li class="active">Experiences</li>
          <li>Diplomes</li>
          <li>Langues</li>
          <li>Confirmation</li>
        </ul>

        <!--EXPERIENCES FIELD SET --> 
        <fieldset>
          <h2 class="fs-title">Experiences</h2>
          <h3 class="fs-subtitle">Veuillez entrez vos experiences</h3>
          <hr>
          <div class="form-row">
            <div class="form-group col-sm-1">
            </div>
            <div class="form-group">
              <label for="dispo">Disponibilite</label><b style="color:#F00"> *</b>
              <select class="form-control" style="width:200px" id="dispo" name="dispo" required>
                <option disabled selected> -- </option>
                <option>Immédiate</option>
                <option>Préavis de 1 moi </option>
                <option>Préavis de 3 mois</option>
                <option>Plus de 3 mois </option>
              </select>
            </div>
          </div>
          <hr>
          <h5 class="text-center"> Avez vous des expériences professionnelles ?</h5>
          <br>
          <div id="experience-content"></div>
          <br>
          <input type="hidden" id="total_experiences" name="total_experiences" value="0"/>
          <input type="button" data-page="1" id="add_experience" name="" class="yes-previous action-button" value="Oui" />
          <input type="button" data-page="1" name="next" id="next-action-please" class="next action-button" value="Non" />
        </fieldset>

        <!-- DIPLOMES FIELD SET -->  
        <fieldset>
          <h2 class="fs-title">Diplomes</h2>
          <h3 class="fs-subtitle">Veuillez entrez vos diplomes</h3>
          <hr>
          <div class="form-diplomes">

                <div class="">
                    <label for="diplome" >Intitulé du diplôme :</label><b style="color:#F00"> *</b>
                    <input id="diplome" class="form-control" required="true"  type="text" name="diplome" /> 
                  </div>

                  <div class="">
                      <label for="etablissement"  id="lbl_etablissement">Etablissement de Formation </label><b style="color:#F00">*</b>
                      <?php echo Form::select("etablissement", $etabs, null, ["class" => "form-control", "id" => "etablissement"]); ?>
                      <label for="autre_etablissement" >Autre Etablissement :</label></b>
                      <input id="autre_etablissement" class="form-control" required="true"  type="text" name="autre_etablissement" /> 
                  </div>

                  <div>
                    <label for="niveau_formation" >Niveau de formation :</label><b style="color:#F00"> *</b>
                    <?php echo Form::select("niveau_formation", $niveau_formations, null, ["class" => "form-control", "id" => "niveau_formation"]); ?>
                  </div>

                  <div>
                    <label for="specialite" id="lbl_specialite" >Spécialité :</label></b>
                    <?php echo Form::select("specialite", $specialites, null, ["class" => "form-control", "id" => "specialite"]); ?>
                    <label for="autre_specialite" >Autre Spécialité :</label></b>
                    <input id="autre_specialite" class="form-control" required="true"  type="text" name="autre_specialite" /> 
                  </div>

          </div>
          <br>
          <input type="button" data-page="2" name="previous" class="previous action-button" value="Précédent" />
          <input type="button" data-page="2" name="next" class="next action-button" value="Suivant" />
        </fieldset>

        <!-- LANGUES FIELD SET -->  
        <fieldset>
          <h2 class="fs-title">Langues</h2>
          <h3 class="fs-subtitle">Veuillez entrez vos langues</h3>
          <br>
          <!-- LANGUES CONTENT DIV STARTS HERE -->  
          <div id="langues-content"> 

            <!-- LANGUE FORM 1 -->  
            <div id="form-langues_1" class="form-langues_1">
              <div class="form-row">
                <div class="form-group col-sm-6">
                  <label for="langue_1" >Langue : </label><b style="color:#F00"> *</b>
                  <?php echo Form::select("langue_1", $langues, null, ["class" => "form-control"]); ?>
                </div>
                <div class="form-group col-sm-6">
                  <label for="niveau_1" >Niveau : </label><b style="color:#F00"> *</b>
                  <?php echo Form::select("niveau_1", $niveau_langues, null, ["class" => "form-control"]); ?>
                </div>
              </div>
            </div>

            <!-- LANGUE FORM 2 -->  
            <div id="form-langues_2" class="form-langues_2">
              <div class="form-row">
                <div class="form-group col-sm-6">
                  <label for="langue_2" >Langue : </label><b style="color:#F00"> *</b>
                  <?php echo Form::select("langue_2", $langues, 3, ["class" => "form-control"]); ?>
                </div>
                <div class="form-group col-sm-6">
                  <label for="niveau_2" >Niveau : </label><b style="color:#F00"> *</b>
                  <?php echo Form::select("niveau_2", $niveau_langues, null, ["class" => "form-control"]); ?>
                </div>
              </div>
            </div>

            <!-- LANGUE FORM 3 -->  
            <div id="form-langues_3" class="form-langues_3">
              <div class="form-row">
                <div class="form-group col-sm-6">
                  <label for="langue_3" >Langue : </label><b style="color:#F00"> *</b>
                  <?php echo Form::select("langue_3", $langues, 4, ["class" => "form-control"]); ?>
                </div>
                <div class="form-group col-sm-6">
                  <label for="niveau_3" >Niveau : </label><b style="color:#F00"> *</b>
                  <?php echo Form::select("niveau_3", $niveau_langues, null, ["class" => "form-control"]); ?>
                </div>
              </div>
            </div>

          </div>
          <!-- LANGUES CONTENT DIV ENDS HERE -->  
          <hr>
          <h5 class="text-center"><span class="lan">Voulez vous ajoutez une autre langue ?</span>
            <a id="add_langues" style="width: 100px;" class="add-more-langues-link"><i class="fa fa-info-circle">     </i> oui </a>
          </h5>
          <br>

          <input type="hidden" id="total_langues" name="total_langues" value="3"/>
          <input type="button" data-page="3" name="previous" class="previous action-button" value="Précédent" />
          <input type="button" data-page="3" name="next" class="next action-button" value="Suivant" />
        </fieldset>

        <!-- CONFIRMATION FIELD SET -->  
        <fieldset>
          <h2 class="fs-title">Confirmation</h2>
          <h3 class="fs-subtitle">Veuillez verifiez vos donner aprés l'envoie du formulaire</h3>
          <input type="button" data-page="3" name="previous" class="previous action-button" value="Précédent" />
          <input id="submit" class="hs-button primary large action-button next conf" type="submit" value="Postuler">
      </fieldset>
    </form>

  </div>

</div>
@endsection