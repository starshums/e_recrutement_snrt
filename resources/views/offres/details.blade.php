@extends('layouts.app')

@section('content')
<div class="container">

    <div class="container-header clearfix">
        <h2> <i class="fa fa-file-alt"> </i> Offre N° {{ $offre->id_offre }} // Code Operation N°
            {{ $offre->code_operation }} </h2>
    </div>

    <div class="container-menu text-center">
        @if ( !Auth::user()->profile->suivi_offres->find($offre->id_offre) )
        <a href="../../offres/postuler/{{ $offre->id_offre }}" class="container-details-link-blue p-2 ">
            <strong> <i class="fa fa-file"> </i> Postuler à cette offre</strong> </a>
        @endif
    </div>

    <div class="container-table">
        <h3 style="color: #444444;"><i class="fa fa-info-circle"> </i> Détails</h3>
        <table class="table-hover bl">
            <tr>
                <td>Operation : </td>
                <td> {{ $offre->operation }} </td>
            </tr>
            <tr>
                <td>Code Operation : </td>
                <td> {{ $offre->code_operation }} </td>
            </tr>
            <tr>
                <td>Métier : </td>
                <td> {{ $offre->metier }} </td>
            </tr>
            <tr>
                <td>Nombre de Postes : </td>
                <td> {{ $offre->nbr_postes }} </td>
            </tr>
            <tr>
                <td>Date de publication : </td>
                <td> {{ $offre->date_offre }} </td>
            </tr>
            <tr>
                <td>Date Delai : </td>
                <td> {{ $offre->date_delai }} </td>
            </tr>
            <hr>
        </table>
        <br>
        <hr>
        <table>

            <tr>
                <td>Fichier de l'annonce : </td>
                <td> <a target="_blank" class="container-details-link"
                        href="../../../storage/app/OFFRES/{{$offre->id_offre}}/publication/{{$offre->fichier_offre}}">
                        <i class="fa fa-cloud-download-alt"> </i>
                        <span class="container-details-text"> Télécharger </span>
                    </a>
                </td>
            </tr>
        </table>
        <hr>
        <table>
            <tr>
                <td>Fichiers de résultats : </td>
                @if( $offre->offres_resultats )
                    @if (Auth::user()->hasRole("ADMIN") == 1)
                    <td>
                        <a class="btn-container-result-file" data-toggle="modal" data-target="#OffreFileResultModal"
                            href="">
                            <i class="fa fa-file"> </i>
                            <span class="container-details-text"> Ajouter résultat</span>
                        </a>
                    </td>
                    @endif
                @endif
            </tr>
        </table> <br>
        @if( $offre->offres_resultats )
            <table class="container text-center">
                @foreach ( $offre->offres_resultats as $fichier_resultat )
                    <tr>
                        <td>
                            <a target="_blank" class="container-details-link-blue"
                                href="../../../storage/app/OFFRES/{{ $offre->id_offre }}/resultat/{{ $fichier_resultat->fichier_resultat }}">
                                <i class="fa fa-cloud-download-alt"> </i>
                                <span class="container-details-text"> {{ $fichier_resultat->description }} </span>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        @endif
        <br>
    </div>

</div>

<!--MODAL AJOUTER FICHIER RESULTAT STARTS HERE -->
<div class="modal fade" id="OffreFileResultModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-info-circle"> </i> Ajouter fichier
                    résultat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('offre.ajouter.fichier_resultat') }}"
                    enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form">

                        <input type="hidden" value="{{$offre->id_offre}}" name="id_offre">

                        <div class="form-row">
                            <div class="form-group col-sm-6">
                                <label for="file_result">Choisir le fichier résultat : </label><b style="color:#F00">
                                    *</b>
                                <input type="file" name="file_result" id="file_result">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <label for="description_fichier_resultat">Description : </label><b style="color:#F00">
                                    *</b>
                                <input type="text" name="description_fichier_resultat" id="description_fichier_resultat"
                                    placeholder="ex : Liste des candidats retenus pour passer les tests écrit"
                                    class="form-control">
                            </div>
                        </div>

                    </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="container-details-link" data-dismiss="modal"><i class="fa fa-window-close">
                    </i> Annuler</button>
                <button type="submit" class="container-details-link"><i class="fa fa-save"> </i>
                    Enregistrer</button>
                </form>
            </div>

        </div>
    </div>
</div>
<!--MODAL AJOUTER FICHIER RESULTAT ENDS HERE -->

@endsection