@extends('layouts.app')

@section('content')
<div class="container">

<div class="container-table">

                    <h3 style="color: #444444;"><i class="fa fa-info-circle">         </i>  Reçu</h3>
                    <hr>

                    <form action="{{ route('profile.telecharger') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                        <input type="hidden" name="id_profile" id="id_profile" value="{{ $profil->id_profil }}">
                        <input type="hidden" name="id_offre" id="id_offre" value="{{ $offre->id_offre }}">

                        <table class="table-hover bl">
                            <tr>
                                <td><strong>Nom Complet : </strong></td>
                                <td> {{ Auth::user()->nom }} {{ Auth::user()->prenom }} </td>
                            </tr>
                            <tr>
                                <td><strong>Cin : </strong></td>
                                <td> {{ Auth::user()->cin }} </td>
                            </tr>
                            <tr>
                                <td><strong>Opération : </strong></td>
                                <td> {{ $offre->operation}} </td>
                            </tr>
                            <tr>
                                <td><strong>Offre : </strong></td>
                                <td> {{ $offre->metier }} </td>
                            </tr>
                            <tr>
                                <td><strong>Etablissement de Formation : </strong></td>
                                <td>
                                    @if( $profil->etablissment->id_etab != 1 )
                                        {{ $profil->etablissment->nom }}
                                    @else
                                        {{ $profil->autre_etablissement }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Niveau de Formation : </strong></td>
                                <td> {{ $profil->niveau->nom }} </td>
                            </tr>
                            <tr>
                                <td><strong>Spécialité : </strong></td>
                                <td>
                                    @if( $profil->specialite->id_specialite != 1 )
                                        {{ $profil->specialite->nom }}
                                    @else
                                        {{ $profil->autre_specialite }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Intitulé du diplôme : </strong></td>
                                <td> {{ $profil->diplome }} </td>
                            </tr>
                        </table>

                        <hr>
                        <div class="container text-center">

                            <div class="form-group text-center">
                                <button target="_blank" type="submit" class="container-details-link-blue">
                                    <i class="fa fa-save"> </i> Imprimer le Reçu</button>
                            </div>

                        </div>

                        </form>

                </div>
</div>
                
                

@endsection