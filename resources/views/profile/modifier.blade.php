@extends('layouts.app')

@section('content')
<div class="container">
        <div class="container-header clearfix">
            <h2> <i class="fa fa-user">         </i>    Modification du profile  </h2>
        </div>

        <div class="container-table">
        <div id="formContent">
                                
        <h3 style="color: #444444;"><i class="fa fa-user">         </i>  Modifier Profile</h3>
        <hr>
                                <!-- Login Form -->
                                <form class="fadeIn first" method="POST" action="{{ route('profile.modifier') }}">
                                    {{ csrf_field() }}
                                        <div class="form-row">

                                            <!-- <div class="col-md-3 {{ $errors->has('cin') ? ' has-error' : '' }}">
                                                <label for="cin" class="control-label">CIN</label>
                                                <input id="cin" type="text" class="form-control" name="cin" value="{{ Auth::user()->cin }}" required autofocus>

                                                @if ($errors->has('cin'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('cin') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Exemple AB9999 (*)</small>
                                            </div> -->

                                            <div class="col-md-6 {{ $errors->has('nom') ? ' has-error' : '' }}">
                                                <label for="nom" class="control-label">Votre Nom</label>
                                                <input id="nom" type="text" class="form-control" name="nom" value="{{ Auth::user()->nom }}" required autofocus>

                                                @if ($errors->has('nom'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('nom') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Nom en majuscule (*)</small>
                                            </div>

                                            <div class="col-md-6 {{ $errors->has('prenom') ? ' has-error' : '' }}">
                                                <label for="prenom" class="control-label">Votre Prénom</label>
                                                <input id="prenom" type="text" class="form-control" name="prenom" value="{{ Auth::user()->prenom }}" required autofocus>

                                                @if ($errors->has('prenom'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('prenom') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Prénom en majuscule (*)</small>
                                            </div>

                                        </div>

                                        <br>
                                        <div class="form-row">
                                            
                                            <div class="col-md-6 {{ $errors->has('date_naissance') ? ' has-error' : '' }}">
                                                <label for="date_naissance" class="control-label">Date de Naissance</label>
                                                <input id="date_naissance" type="text" class="form-control datepicker" data-provide="datepicker" name="date_naissance" value="{{ Auth::user()->date_naissance }}" required autofocus>

                                                @if ($errors->has('date_naissance'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('date_naissance') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;"> (AAAA/MM/JJ) (*)</small>
                                            </div>

                                            <div class="col-md-6 {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                                <label for="telephone" class="control-label">Téléphone</label>
                                                <input id="telephone" type="text" class="form-control" name="telephone" value="{{ Auth::user()->telephone }}" required autofocus>

                                                @if ($errors->has('telephone'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('telephone') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Téléphone (*)</small>
                                            </div>

                                        </div>

                                        <br>
                                        <div class="form-row">

                                            <div class="col-md-12 {{ $errors->has('adresse') ? ' has-error' : '' }}">
                                                <label for="adresse" class="control-label">Adresse</label>
                                                <input id="adresse" type="text" class="form-control" name="adresse" value="{{ Auth::user()->adresse }}" required autofocus>

                                                @if ($errors->has('adresse'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('adresse') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Adresse (*)</small>
                                            </div>
                                        </div>


                                        <br>
                                        <div class="form-row">
                                            <div class="col-md-12 {{ $errors->has('ville') ? ' has-error' : '' }}">
                                                <label for="ville" class="control-label">Ville</label>
                                                <input id="ville" type="text" class="form-control" name="ville" value="{{ Auth::user()->ville }}" required autofocus>

                                                @if ($errors->has('ville'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('ville') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Ville (*)</small>
                                            </div>
                                        </div>
                                        
                                        <br>
                                        <div class="form-row">

                                            <div class="col-md-12 {{ $errors->has('pays') ? ' has-error' : '' }}">
                                                <label for="pays" class="control-label">Pays</label>
                                                <input id="pays" type="text" class="form-control" name="pays" value="{{ Auth::user()->pays }}" required autofocus>

                                                @if ($errors->has('pays'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('pays') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Pays (*)</small>
                                            </div>

                                            <!-- <div class="col-md-8 {{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="control-label">Adress e-mail</label>
                                                <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                                <small id="" class="form-text text-muted" style="color: red;">Adresse e-mail (*) (**)</small>
                                            </div> -->

                                        </div>

                                        <br>
                                        <button type="submit" class="btn-container mod"> <i class="fa fa-sign-in-alt">     </i> Enregistrer</button>
                                      </form>
                            
                              </div>
                      
                </div>
        </div>
                    
</div>
@endsection




