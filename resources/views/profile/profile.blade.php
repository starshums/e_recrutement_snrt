@extends('layouts.app')

@section('content')
<div class="container clearfix">

    <div class="container-header clearfix">
        <h2> <i class="fa fa-file-alt"> </i> Profile // {{ $user->nom }} {{ $user->prenom }} </h2>
    </div>

    <div class="container-table">

        <div class="container col-sm-12 clearfix">

            
                @if (Auth::user()->hasRole("ADMIN") == 1)
                    <h3 style="background: #191c40; color: white; padding: 8px 15px;">
                @else
                    <h3 style="background: #444444; color: white; padding: 8px 15px;">
                @endif
                <i class="fa fa-user"> </i> Mes Informations</h3>
                <div class="container-menu text-center">
                    <a href="../profile/modifier" class="container-details-link container-details-link-blue p-2"> <strong> <i class="fa fa-edit">
                            </i> Modifier </strong> </a>
                </div>
                <table class="table-hover bl">
                    <tr>
                        <td>Nom : </td>
                        <td> {{ $user->nom }} </td>
                    </tr>
                    <tr>
                        <td>Prénom : </td>
                        <td> {{ $user->prenom }} </td>
                    </tr>
                    <tr>
                        <td>CIN : </td>
                        <td> {{ $user->cin }} </td>
                    </tr>
                    <tr>
                        <td>Email : </td>
                        <td> {{ $user->email }} </td>
                    </tr>
                    <tr>
                        <td>Date de Naissance : </td>
                        <td> {{ $user->date_naissance }} </td>
                    </tr>
                    <tr>
                        <td>Téléphone : </td>
                        <td> {{ $user->telephone }} </td>
                    </tr>
                    <tr>
                        <td>Adresse : </td>
                        <td> {{ $user->adresse }} </td>
                    </tr>
                    <tr>
                        <td>Ville : </td>
                        <td> {{ $user->ville }} </td>
                    </tr>
                    <tr>
                        <td>Pays : </td>
                        <td> {{ $user->pays }} </td>
                    </tr>
                </table>
            </div>

            <!-- <div class="float-left  col-sm-7  profil-floated-box prof">
                @if (Auth::user()->hasRole("ADMIN") == 1)
                    <h3 style="background: #191c40; color: white; padding: 8px 15px;">
                @else
                    <h3 style="background: #444444; color: white; padding: 8px 15px;">
                @endif
                <i class="fa fa-suitcase"> </i> Mes Offres</h3>
                @if( $profil->suivi_offres->get(0) != null )
                <table class="table-hover">
                    <thead>
                        <tr>
                            <th width="" style="font-size:12,5px" scope="col">Code Opération</th>
                            <th width="" style="font-size:12,5px" scope="col">Opération</th>
                            <th width="" style="font-size:12,5px" scope="col">Métier</th>
                            <th width="" style="font-size:12,5px" scope="col">Date Publication</th>
                        </tr>
                    </thead>
                    <tbody class="table-hover">
                        @foreach ( $profil->suivi_offres as $offre )
                        <tr>
                            <td width="200" style="font-size:12,5px" scope="col">{{ $offre->code_operation }}</td>
                            <td width="200" style="font-size:12,5px" scope="col">{{ $offre->operation }}</td>
                            <td width="200" style="font-size:12,5px" scope="col">{{ $offre->metier }}</td>
                            <td width="100" style="font-size:12,5px" scope="col">{{ $offre->date_offre }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <div class="container text-center">
                        <h6> <i class="fa fa-file">  </i> rien à afficher ...</h6>
                    </div>
                @endif
            </div> -->

        
    </div>
    </div>


    @endsection