@extends('layouts.app')

@section('content')
<div class="container clearfix">

                <div class="container-header clearfix">
                    <h2> <i class="fa fa-file-alt">         </i>    Profile // {{ $user->nom }} {{ $user->prenom }} </h2>
                </div>

                <div class="container-table">

                <div class="container col-sm-12 clearfix">

                    <div class="float-left col-sm-4">

                        <h3 style="background: #444444; color: white; padding: 8px 15px;"><i class="fa fa-link">         </i>  Statut</h3>
                        <table class="table-hover">
                            <thead>
                                <tr>
                                    <th width="" style="font-size:12,5px" scope="col">Disponibilité</th>
                                    <th width="" style="font-size:12,5px" scope="col">Salaire</th>
                                    <th>action</th>
                                </tr>
                                </thead>
                            <tbody class="table-hover">
                                <tr>
                                    <td width="200" style="font-size:12,5px" scope="col">{{ $profil->disponibilite}}</td>
                                    <td width="200" style="font-size:12,5px" scope="col">{{ $profil->salaire }}</td>
                                    <td width="30" style="font-size:12,5px" scope="col">
                                    <button type="button" class="container-details-link" data-toggle="modal" data-target="#modal_modifier_status">
                                        <i class="fa fa-edit">         </i>
                                    </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <br>
                        <h3 style="background: #444444; color: white; padding: 8px 15px;"><i class="fa fa-user">         </i>  Mon Informations</h3>
                        <div class="container-menu text-center">
                            <a href="../profile/modifier" class="container-details-link c-d-l-2"> <strong> <i class="fa fa-edit">         </i> Modifier mon profile </strong> </a>
                        </div>
                        <table class="table-hover">
                            <tr>
                                <td>Nom : </td>
                                <td> {{ $user->nom }} </td>
                            </tr>
                            <tr>
                                <td>Prénom : </td>
                                <td> {{ $user->prenom }} </td>
                            </tr>
                            <tr>
                                <td>CIN : </td>
                                <td> {{ $user->cin }} </td>
                            </tr>
                            <tr>
                                <td>Email : </td>
                                <td> {{ $user->email }} </td>
                            </tr>
                            <tr>
                                <td>Date de Naissance : </td>
                                <td> {{ $user->date_naissance }} </td>
                            </tr>
                            <tr>
                                <td>Téléphone : </td>
                                <td> {{ $user->telephone }} </td>
                            </tr>
                            <tr>
                                <td>Adresse : </td>
                                <td> {{ $user->adresse }} </td>
                            </tr>
                            <tr>
                                <td>Ville : </td>
                                <td> {{ $user->ville }} </td>
                            </tr>
                            <tr>
                                <td>Pays : </td>
                                <td> {{ $user->pays }} </td>
                            </tr>
                        </table>

                        <br>
                        <h3 style="background: #444444; color: white; padding: 8px 15px;"><i class="fa fa-globe-africa ">         </i>  Mes Langues</h3>
                        <table class="table-hover">
                            <tbody class="table-hover">
                                @foreach ( $profil->profil_niveau_langues as $pnl )
                                <tr>
                                    <td width="200" style="font-size:12,5px" scope="col">{{ $pnl->langue->nom}}</td>
                                    <td width="200" style="font-size:12,5px" scope="col">{{ $pnl->niveau_langue->niveau }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    
                    <div class="float-left  col-sm-8  profil-floated-box">
                        <h3 style="background: #444444; color: white; padding: 8px 15px;"><i class="fa fa-suitcase">         </i>  Mes Offres</h3>
                        <table class="table-hover">
                            <thead>
                                <tr>
                                    <th width="" style="font-size:12,5px" scope="col">Code Opération</th>
                                    <th width="" style="font-size:12,5px" scope="col">Opération</th>
                                    <th width="" style="font-size:12,5px" scope="col">Métier</th>
                                    <th width="" style="font-size:12,5px" scope="col">Date Publication</th>
                                </tr>
                                </thead>
                            <tbody class="table-hover">
                                @foreach ( $profil->suivi_offres as $offre )
                                <tr>
                                    <td width="200" style="font-size:12,5px" scope="col">{{ $offre->code_operation }}</td>
                                    <td width="200" style="font-size:12,5px" scope="col">{{ $offre->operation }}</td>
                                    <td width="200" style="font-size:12,5px" scope="col">{{ $offre->metier }}</td>
                                    <td width="100" style="font-size:12,5px" scope="col">{{ $offre->date_offre }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="float-left  col-sm-8  profil-floated-box">
                        <h3 style="background: #444444; color: white; padding: 8px 15px;"><i class="fa fa-user-graduate">         </i>  Mon Diplomes </h3>
                        <table class="table-hover">
                            <thead>
                                <tr>
                                <th style="font-size:12,5px" scope="col">Intitulé du Diplome</th>
                                <th style="font-size:12,5px" scope="col">Niveau de formation</th>
                                <th width="300" style="font-size:12,5px" scope="col">Spécialité</th>
                                <th width="300" style="font-size:12,5px" scope="col">Etablissement</th>
                                <th width="50" style="font-size:12,5px" scope="col">action</th>
                                </tr>
                                </thead>
                            <tbody class="table-hover">
                                <tr>
                                    <td> {{ $profil->diplome }} </td>
                                    <td> {{ $profil->niveau->nom }} </td>
                                    <td>
                                    @if(  $profil->specialite->id_specialite != 1 )
                                        {{ $profil->specialite->nom }}
                                    @else
                                        {{ $profil->autre_specialite }}
                                    @endif
                                    </td>
                                    <td>
                                    @if(  $profil->etablissment->id_etab != 1 )
                                        {{ $profil->etablissment->nom }}
                                    @else
                                        {{ $profil->autre_etablissement }}
                                    @endif
                                    </td>
                                    <td>
                                    <button type="button" class="container-details-link" data-toggle="modal" data-target="#modal_modifier_diplomes">
                                        <i class="fa fa-edit">         </i>
                                    </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="float-left  col-sm-8 profil-floated-box">
                        <h3 style="background: #444444; color: white; padding: 8px 15px;"><i class="fa fa-suitcase">         </i>  Mes Experiences Professionelles</h3>
                        <table class="table-hover">
                            <thead>
                                <tr>
                                <th style="font-size:12,5px" scope="col">Poste</th>
                                <th style="font-size:12,5px" scope="col">Employeur</th>
                                <th style="font-size:12,5px" scope="col">Secteur</th>
                                <th style="font-size:12,5px" scope="col">Date Début</th>
                                <th style="font-size:12,5px" scope="col">Date Fin</th>
                                <th width="50" style="font-size:12,5px" scope="col">action</th>
                                </tr>
                                </thead>
                            <tbody class="table-hover">
                                @foreach ( $profil->experiences as $experience )
                                <tr>
                                    <td> {{ $experience->poste }} </td>
                                    <td> {{ $experience->employeur }} </td>
                                    <td> {{ $experience->secteur }} </td>
                                    <td> {{ $experience->date_debut }} </td>
                                    <td> {{ $experience->date_fin }} </td>
                                    <td width="30" style="font-size:12,5px" scope="col">
                                    <button type="button" class="container-details-link" data-toggle="modal" data-target="#exp_{{$loop->index}}_exp">
                                        <i class="fa fa-edit">         </i>
                                    </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <!-- MODAL MODIFIER DIPLOMES STARTS HERE -->
<div class="modal fade" id="modal_modifier_status">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_modifier_statusLabel"><i class="fa fa-info-circle">         </i>  Modifier mon status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('profile.modifier_status') }}">
        {{ csrf_field() }}
        <div class="form-diplomes">

        <div class="form-row">
            <div class="form-group col-sm-6" style="margin-right: 25px;">
                <label for="salaire">Salaire Net Actuel : </label>
                <p><strong>({{ $profil->salaire }})</strong></p>
                <select class="form-control" style="width:250px" id="salaire" name="salaire">
                  <option disabled selected> -- </option>
                  <option>inferieur ou égal à 3000 DH</option>
                  <option>3000DH ; 5000DH</option>
                  <option>5000DH ; 7000DH</option>
                  <option>7000DH ; 10000DH</option>
                  <option>10000DH ; 15000DH</option>
                  <option>supérieur à 15000DH </option>
                </select>
            </div>
            <div class="form-group col-sm-5">
              <label for="dispo">Disponibilite :</label><b style="color:#F00"> *</b>
              <p><strong>({{ $profil->disponibilite }})</strong></p>
              <select class="form-control" style="width:200px" id="dispo" name="dispo">
                <option disabled selected> -- </option>
                <option>Immédiate</option>
                <option>Préavis de 1 moi </option>
                <option>Préavis de 3 mois</option>
                <option>Plus de 3 mois </option>
              </select>
            </div>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="container-details-link" data-dismiss="modal"><i class="fa fa-window-close">         </i>  Annuler</button>
        <button type="submit" class="container-details-link"><i class="fa fa-save">         </i>  Enregistrer</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- MODAL MODIFIER DIPLOMES ENDS HERE -->

<!-- MODAL MODIFIER DIPLOMES STARTS HERE -->
<div class="modal fade" id="modal_modifier_diplomes">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_modifier_diplomesLabel"><i class="fa fa-info-circle">         </i>  Modifier mes diplomes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('profile.modifier_diplomes') }}">
        {{ csrf_field() }}
        <div class="form-diplomes">

            <div class="">
                <label for="diplome" >Intitulé du diplôme :</label><b style="color:#F00"> *</b>
                <input id="diplome" class="form-control" required="true" value="{{ $profil->diplome }}"  type="text" name="diplome" /> 
            </div>

            <div class="">
                <label for="etablissement"  id="lbl_etablissement">Etablissement de Formation </label><b style="color:#F00">*</b>
                <?php echo Form::select("etablissement", $etabs, $profil->etablissment->id_etab, ["class" => "form-control", "id" => "etablissement"]); ?>
                <label for="autre_etablissement" >Autre Etablissement :</label></b>
                <input id="autre_etablissement" class="form-control" required="true" value="{{ $profil->autre_etablissement }}"  type="text" name="autre_etablissement" /> 
            </div>

            <div>
                <label for="niveau_formation" >Niveau de formation :</label><b style="color:#F00"> *</b>
                <?php echo Form::select("niveau_formation", $niveau_formations, $profil->niveau->id_niveau, ["class" => "form-control", "id" => "niveau_formation"]); ?>
            </div>

            <div>
                <label for="specialite" id="lbl_specialite" >Spécialité :</label></b>
                <?php echo Form::select("specialite", $specialites, $profil->specialite->id_specialite, ["class" => "form-control", "id" => "specialite"]); ?>
                <label for="autre_specialite" >Autre Spécialité :</label></b>
                <input id="autre_specialite" class="form-control" required="true" value="{{ $profil->autre_specialite }}"  type="text" name="autre_specialite" /> 
            </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="container-details-link" data-dismiss="modal"><i class="fa fa-window-close">         </i>  Annuler</button>
        <button type="submit" class="container-details-link"><i class="fa fa-save">         </i>  Enregistrer</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- MODAL MODIFIER DIPLOMES ENDS HERE -->

@foreach ( $profil->experiences as $experience )
<div class="modal fade" id="#exp_{{$loop->index}}_exp">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal_modifier_diplomesLabel"><i class="fa fa-info-circle">         </i>  Modifier mes diplomes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('profile.modifier_diplomes') }}">
        {{ csrf_field() }}
        <div class="form-diplomes">

            <div class="">
                <label for="diplome" >Intitulé du diplôme :</label><b style="color:#F00"> *</b>
                <input id="diplome" class="form-control" required="true" value="{{ $profil->diplome }}"  type="text" name="diplome" /> 
            </div>

            <div class="">
                <label for="etablissement"  id="lbl_etablissement">Etablissement de Formation </label><b style="color:#F00">*</b>
                <?php echo Form::select("etablissement", $etabs, $profil->etablissment->id_etab, ["class" => "form-control", "id" => "etablissement"]); ?>
                <label for="autre_etablissement" >Autre Etablissement :</label></b>
                <input id="autre_etablissement" class="form-control" required="true" value="{{ $profil->autre_etablissement }}"  type="text" name="autre_etablissement" /> 
            </div>

            <div>
                <label for="niveau_formation" >Niveau de formation :</label><b style="color:#F00"> *</b>
                <?php echo Form::select("niveau_formation", $niveau_formations, $profil->niveau->id_niveau, ["class" => "form-control", "id" => "niveau_formation"]); ?>
            </div>

            <div>
                <label for="specialite" id="lbl_specialite" >Spécialité :</label></b>
                <?php echo Form::select("specialite", $specialites, $profil->specialite->id_specialite, ["class" => "form-control", "id" => "specialite"]); ?>
                <label for="autre_specialite" >Autre Spécialité :</label></b>
                <input id="autre_specialite" class="form-control" required="true" value="{{ $profil->autre_specialite }}"  type="text" name="autre_specialite" /> 
            </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="container-details-link" data-dismiss="modal"><i class="fa fa-window-close">         </i>  Annuler</button>
        <button type="submit" class="container-details-link"><i class="fa fa-save">         </i>  Enregistrer</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach













                </div>
            </div>
            
@endsection




