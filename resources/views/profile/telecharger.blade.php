<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>TELECHARGER RECU | E-RECRUTEMENT</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
</head>
<body>

    <div class="wrapper">
        <div class="container">

        <br><br>
            <div class="container text-center">
            <div class="logo">
                    <a href="{{ url('/home') }}">
                        <img class="" id="snrt_logo" src="{{ asset('img/snrt_ins_logo.png') }}" alt="SNRT LOGO" height="100px" >
                    </a>
                </div>
                <div class="logo-text">
                        <h6>e-Recrutement</h6>
                </div>
        </div>
        </div>
        
        <br><br><br><br><br><br><br>
        <div class="container text-center">
            <h3>Reçu d'inscription</h3>

            <br>
            <table class="table text-left">
                            <tr>
                                <td><strong>Nom Complet : </strong></td>
                                <td> {{ Auth::user()->nom }} {{ Auth::user()->prenom }} </td>
                            </tr>
                            <tr>
                                <td><strong>CIN : </strong></td>
                                <td> {{ Auth::user()->cin }} </td>
                            </tr>
                            <tr>
                                <td><strong>Opération : </strong></td>
                                <td> {{ $offre->operation }} </td>
                            </tr>
                            <tr>
                                <td><strong>Offre : </strong></td>
                                <td> {{ $offre->metier }} </td>
                            </tr>
                            <tr>
                                <td><strong>Etablissement de Formation : </strong></td>
                                <td>
                                    @if( $profil->etablissment->id_etab != 1 )
                                        {{ $profil->etablissment->nom }}
                                    @else
                                        {{ $profil->autre_etablissement }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Niveau de Formation : </strong></td>
                                <td> {{ $profil->niveau->nom }} </td>
                            </tr>
                            <tr>
                                <td><strong>Spécialité : </strong></td>
                                <td>
                                    @if( $profil->specialite->id_specialite != 1 )
                                        {{ $profil->specialite->nom }}
                                    @else
                                        {{ $profil->autre_specialite }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Intitulé du diplôme : </strong></td>
                                <td> {{ $profil->diplome }} </td>
                            </tr>
                        </table>
                        <br>
                        <h3> {!!DNS1D::getBarcodeHTML( Auth::user()->cin . ' / ' . $offre->operation . '', 'C39')!!} </h3>
        </div>

    <br><br>
    <div class="container text-center">
        <h6>Directeur des Systèmes d'Information Société Nationale de Radiodiffusion et de Télévision 1, Rue EL BRIHI 10000 Rabat Maroc</h6>
        <h6>SNRT © 2019</h6>
    </div>

    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>
    <script src="{{ asset('js/cc.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

</body>
</html>