@extends('layouts.app')

@section('content')
<div class="container">

<div class="container-table">

                    <h3 style="color: #444444;"><i class="fa fa-info-circle">         </i>  Validation</h3>
                    <hr>

                    <form action="{{ route('profile.validation') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                        <input type="hidden" name="id_profile" id="id_profile" value="{{ $get_profile->id_profil }}">
                        <input type="hidden" name="id_offre" id="id_offre" value="{{ $get_offre->id_offre }}">

                        <table class="table-hover bl">
                            <tr>
                                <td><strong>Nom Complet : </strong></td>
                                <td> {{ Auth::user()->nom }} {{ Auth::user()->prenom }} </td>
                            </tr>
                            <tr>
                                <td><strong>Cin : </strong></td>
                                <td> {{ Auth::user()->cin }} </td>
                            </tr>
                            <tr>
                                <td><strong>Opération : </strong></td>
                                <td> {{ $get_offre->operation}} </td>
                            </tr>
                            <tr>
                                <td><strong>Offre : </strong></td>
                                <td> {{ $get_offre->metier }} </td>
                            </tr>
                            <tr>
                                <td><strong>Etablissement de Formation : </strong></td>
                                <td>
                                    @if( $get_profile->etablissment->id_etab != 1 )
                                        {{ $get_profile->etablissment->nom }}
                                    @else
                                        {{ $get_profile->autre_etablissement }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Niveau de Formation : </strong></td>
                                <td> {{ $get_profile->niveau->nom }} </td>
                            </tr>
                            <tr>
                                <td><strong>Spécialité : </strong></td>
                                <td>
                                    @if( $get_profile->specialite->id_specialite != 1 )
                                        {{ $get_profile->specialite->nom }}
                                    @else
                                        {{ $get_profile->autre_specialite }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Intitulé du diplôme : </strong></td>
                                <td> {{ $get_profile->diplome }} </td>
                            </tr>
                        </table>

                        <hr>
                        <div class="form-group">
                            <label for="cv_file"> Joindre votre CV <b style="color:#F00">(Champ Obligatoire en format PDF ) *</b></label><br>
                            <input type="file" name="cv_file" id="cv_file" required>
                        </div>

                        <hr>
                        <div class="form-group text-center" style="width: 400px;">
                            <input type="checkbox" name="valider_all" id="valider_all" required><b style="color:#F00">*</b>
                            <label for="valider_all" class="">Sous peine du rejet de ma candidature j'affirme que toutes les informations saisies sont correctes et justes et ne présentent aucune fausse mention </label>
                        </div>

                        <hr>
                        <div class="form-group text-center">
                            <span class="">
                                <button type="submit"
                                class="container-details-link-blue"   >
                                    <i class="fa fa-save"> </i> Valider et Imprimer le Reçu</button>
                            </span>
                            <a href="../../profile/annuler/{{ $get_profile->id_profil }}" class="container-details-link"><i class="fa fa-window-close"> </i> Annuler</a>
                        </div>
                        

                    </form>

                </div>
</div>
                
                

@endsection