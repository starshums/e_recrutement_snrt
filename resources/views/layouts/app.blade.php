<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SNRT | E-RECRUTEMENT</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    @if (Auth::user()->hasRole("ADMIN") == 1)
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    @elseif (Auth::guest() OR Auth::user()->hasRole("USER") == 1)
        <link rel="stylesheet" href="{{ asset('css/mainUser.css') }}">
    @endif
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
</head>
<body>

    <div class="wrapper">
        
        <div class="header">
            <div class="logo">
                <a href="{{ url('/home_test') }}">
                    <img id="snrt_logo" src="{{ asset('img/snrt_ins_logo.png') }}" alt="SNRT LOGO" height="100px" >
                </a>
            </div>
            <div class="logo-text">
                    <h3>e-Recrutement</h3>
                    <h4> Bienvenue sur l'espace recrutement de la SNRT</h4>
            </div>
            
            <nav class="navbar navbar-expand-lg navbar-light">
      
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                  
                    <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                      <ul class="navbar-nav mr-auto clearfix">
                      <li class="nav-item">
                          <a class="nav-link" href="{{ url('/home_test') }}"><i class="fa fa-home">    </i>    Acceuil </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/offres/lister/p') }}"><i class="fa fa-layer-group">    </i>    Offres Disponibles</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/offres/lister/r') }}"><i class="fa fa-folder">    </i>    Résultats</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('/profile/mon_profile') }}"><i class="fa fa-user">     </i> 
                            @if (Auth::guest())
                                Mr. User
                            @else
                              Mr. {{ Auth::user()->nom }} 
                            @endif
                            </a>
                        </li>
                        <li class="nav-item logout-link">

                            <a  class="nav-link" href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-in-alt">     </i> Déconnexion </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                     {{ csrf_field() }}
                                </form>
                        </li>
                      </ul>
                    </div>
                  </nav>

        </div>

        <div class="main-container">

          <div class="container text-center">

                  <div>
                          @if ( count( $errors ) > 0 )
                              <div class="alert alert-danger">
                                  @foreach ($errors->all() as $error)
                                      <i class="fa fa-info-circle">   </i>  {{ $error }}<br>        
                                  @endforeach
                              </div>
                          @endif
                      </div>

                      <div>
                          @if ($message = Session::get('success'))
                          <div class="alert alert-success alert-block">
                              <button type="button" class="close" data-dismiss="alert">×</button>	
                                  <strong><i class="fa fa-info-circle">   </i>  {{ $message }}</strong>
                          </div>
                          @endif
                      </div>

          </div>
            @yield('content')
        </div>

        <div class="footer">
            e-Recrutement SNRT © 2019
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/cc.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

</body>
</html>