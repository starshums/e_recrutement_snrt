<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OffreResultat extends Model
{
    protected $table = 'offres_resultats';
    protected $fillable = [
        "id_offre",
        "fichier_resultat",
        "description"
    ];
    protected $primaryKey = "id_offres_resultat";

    public function offre() {
        return $this->belongsTo('App/Offre', "id_offre");
    }
}
