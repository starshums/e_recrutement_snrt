<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NiveauLangue extends Model
{
    protected $fillable = [
        "niveau",
    ];

    protected $primaryKey = "id_niveau_langue";
}
