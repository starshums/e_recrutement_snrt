<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use App\Profil;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'cin' => 'required|min:6|max:10',
            'nom' => 'required|max:255',
            'prenom' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
            //'g-recaptcha-response' => 'required|captcha'
        ],[
            'cin.required' => '* Champ Obligatoire',
            'nom.required'  => '* Champ Obligatoire',
            'prenom.required'  => '* Champ Obligatoire',
            'email.required'  => '* Champ Obligatoire',
            'password.required'  => '* Champ Obligatoire',
            'cin.min' => "* Le CIN doit être au moins 8 caractères.",
            'email.unique' => "* Ce e-mail existe déja.",
            'password.confirmed' => "* La confirmation du mot de passe ne correspond pas.",
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'cin' => $data['cin'],
            'nom' => $data['nom'],
            'prenom' => $data['prenom'],
            'date_naissance' => $data['date_naissance'],
            'telephone' => $data['telephone'],
            'adresse' => $data['adresse'],
            'ville' => $data['ville'],
            'pays' => $data['pays'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $profil = new Profil;
        $profil->id_user = $user->id;
        $profil->id_niveau = "1";
        $profil->salaire = "1";
        $profil->disponibilite = "1";
        $profil->id_etab = "1";
        $profil->autre_etablissement = "1";
        $profil->id_specialite = "1";
        $profil->autre_specialite = "1";
        $profil->diplome = "1";
        $profil->cv_file = "none";
        $profil->save();

        $user->roles()->attach(Role::where("nom", "USER")->first()->id_role);

        return $user;
    }
}
