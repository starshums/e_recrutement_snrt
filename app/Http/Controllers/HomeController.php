<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offre;
use App\Profil;
use App\Experience;
use App\SuiviOffre;
use App\Etablissement;
use App\Specialite;
use App\Niveau;
use App\Langue;
use App\User;
use App\ProfilNiveauLangue;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offres = Offre::all()->where("a_afficher", "=", "1");
        $offres_r = Offre::all()->where("fichier_resultat", "!=", "none")->where("a_afficher_r", "=", "1");
        return view("home", compact("offres", "offres_r"));
    }

    public function modifier(Request $request) {

        $user = User::find(\Auth::user()->id);

        $user->nom = $request->get("nom");
        $user->prenom = $request->get("prenom");
        $user->date_naissance = $request->get("date_naissance");
        $user->telephone = $request->get("telephone");
        $user->adresse = $request->get("adresse");
        $user->ville = $request->get("ville");
        $user->pays = $request->get("pays");

        // $user->email = $request->get("email");
        // $user->cin = $request->get("cin");

        $user->save();

        return \Redirect::to("profile/mon_profile")->with("success", "Informations profile bien modifier!");
    }

    public function modifier_diplomes(Request $request) {

        $profil = Profil::where( "id_user", "=", Auth::user()->id )->first();

        if( $request->get("etablissement") == "1" ) {
            $profil->id_etab = $request->get("etablissement");
            $profil->autre_etablissement = $request->get("autre_etablissement");
        } else {
            $profil->id_etab = $request->get("etablissement");
            $profil->autre_etablissement = "NONE";
        }
        if( $request->get("specialite") == "1" ) {
            $profil->id_specialite = $request->get("specialite");
            $profil->autre_specialite = $request->get("autre_specialite");
        } else {
            $profil->id_specialite = $request->get("specialite");
            $profil->autre_specialite = "NONE";
        }

        $profil->id_niveau = $request->get("niveau_formation");
        $profil->diplome = $request->get("diplome");
        $profil->save();

        return \Redirect::to("profile/mon_profile");
    }

    public function modifier_status(Request $request) {
        
        $profil = Profil::where( "id_user", "=", Auth::user()->id )->first();
        $profil->salaire = $request->get("salaire");
        $profil->disponibilite = $request->get("dispo");
        $profil->save();

        return \Redirect::to("profile/mon_profile");
    }

    public function modifier_experiences(Request $request) {

        $index = $request->get("loop_index");
        $experience = Experience::find($request->get("id_experience"));
        $experience->poste = $request->get("poste_" . $index);
        $experience->secteur = $request->get("secteur_" . $index);
        $experience->employeur = $request->get("employeur_" . $index);
        $experience->date_debut = $request->get("date_debut_" . $index);
        $experience->date_fin = $request->get("date_fin_" . $index);
        $experience->save();

        return \Redirect::to("profile/mon_profile");
    }

    public function modifier_langues(Request $request) {

        $index = $request->get("loop_index");
        $profil_niveau_langue = ProfilNiveauLangue::find($request->get("id_profils_niveau_langue"));
        $profil = Profil::where( "id_user", "=", Auth::user()->id )->first();
        $profil_niveau_langue->id_profil = $profil->id_profil;
        $profil_niveau_langue->id_langue = $request->get("langue_" . $index);
        $profil_niveau_langue->id_niveau_langue = $request->get("niveau_" . $index);
        $profil_niveau_langue->save();

        return \Redirect::to("profile/mon_profile");
    }
}
