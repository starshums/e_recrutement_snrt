<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Offre;
use App\OffreResultat;
use App\Etablissement;
use App\Profil;
use App\Langue;
use App\Niveau;
use App\NiveauLangue;
use App\ProfilNiveauLangue;
use App\Specialite;
use App\Experience;
use App\SuiviOffre;
use App\User;
use Auth;
use PDF;

class OffresController extends Controller {

    public function validation_rules() {
        return $rules = [
            "operation" => "required",
            "code_operation" => "required",
            "metier" => "required",
            "file_offre" => "required|mimes:pdf"
        ];
    }

    public function postuler_validation_rules() {
        return $rules = [
            "dispo" => "required",
            "diplome" => "required",
            "etablissement" => "required",
            "specialite" => "required"
        ];
    }

    public function ajouter(Request $request) {

        $validator = \Validator::make($request->all(), $this->validation_rules());

        if( $validator->fails() ) {
            $messages = $validator->messages();
            return \Redirect::to('/offres/ajouter')->withErrors($validator)->withInput($request->all());
        } else {
            $offre = new Offre;
            $offre->operation = $request->get("operation");
            $offre->code_operation = $request->get("code_operation");
            $offre->metier = $request->get("metier");
            $offre->nbr_postes = $request->get("nbr_postes");
            $offre->date_offre = $request->get("date_offre");
            $offre->date_delai = $request->get("date_delai");

            $fichier_offre = $request->file("file_offre");
            if( $fichier_offre == null ) {
                $offre->fichier_offre = "none";
            } else {
                $offre->fichier_offre = $fichier_offre->hashName();
            }

            $offre->save();

            if( $fichier_offre != null ) {
                $fichier_offre->storePubliclyAs("OFFRES/" . $offre->id_offre . "/publication/", $fichier_offre->hashName(), "public");
                \Storage::disk('local')->put("OFFRES/" . $offre->id_offre . "/publication/", $fichier_offre);
            }
            
            return \Redirect::to("/offres/p")->with('success','Offre bien ajoutée!');
        }
    }

    public function ajouter_fichier_resultat(Request $request) {
        $validator = \Validator::make($request->all(), ["file_result" => "required|mimes:pdf"]);

        if( $validator->fails() ) {
            $messages = $validator->messages();
            return \Redirect::to('/offres/p')->withErrors($validator)->withInput($request->all());
        } else {
            $fichier_resultat = $request->file("file_result");
            $id_offre = $request->get("id_offre");
            $offre_resultat = new OffreResultat;
            $offre_resultat->id_offre = $id_offre;
            $offre_resultat->fichier_resultat = $fichier_resultat->hashName();
            $offre_resultat->description = $request->get("description_fichier_resultat");
            $offre_resultat->save();
            $fichier_resultat->storePubliclyAs("OFFRES/" . $id_offre . "/resultat/", $fichier_resultat->hashName(), "public");
            \Storage::disk('local')->put("OFFRES/" . $id_offre . "/resultat/", $fichier_resultat);
            return \Redirect::back()->with('success','Fichier résultat bien ajoutée!');
        }
    }

    public function postuler(Request $request, $id_offre) {

        $total_langues = $request->get("total_langues");

        // dd( $total_langues );

        $validator = \Validator::make($request->all(), $this->postuler_validation_rules());

        if( $validator->fails() ) {
            $messages = $validator->messages();
            return \Redirect::to('/offres/postuler/' . $id_offre)->withErrors($validator)->withInput($request->all());
        } else {
            $total_exp = $request->get("total_experiences");
            $total_langues = $request->get("total_langues");
            
            // ADD PROFIL
            $profil = Profil::where( "id_user", "=", Auth::user()->id )->first();
            $profil->id_user = Auth::user()->id;
            $profil->id_niveau = $request->get("niveau_formation");
            $profil->salaire = "0";
            $profil->disponibilite = $request->get("dispo");

            // ADD ETABLISSEMENT && SPECIALITES
            if( $request->get("etablissement") == "1" ) {
                $profil->id_etab = $request->get("etablissement");
                $profil->autre_etablissement = $request->get("autre_etablissement");
            } else {
                $profil->id_etab = $request->get("etablissement");
                $profil->autre_etablissement = "NONE";
            }
            if( $request->get("specialite") == "1" ) {
                $profil->id_specialite = $request->get("specialite");
                $profil->autre_specialite = $request->get("autre_specialite");
            } else {
                $profil->id_specialite = $request->get("specialite");
                $profil->autre_specialite = "NONE";
            }

            // PROFIL CONTINUES
            $profil->diplome = $request->get("diplome");
            $profil->cv_file = "none";
            $profil->save();

            // ADD EXPERIENCES //
            if( $total_exp == 0 ) {
            // dd("has no exp");
            } else {
                for ($i=1; $i <= (int)$total_exp; $i++) {
                    $experience = new Experience;
                    $experience->id_profil = $profil->id_profil;
                    $experience->poste = $request->get("poste_" . $i);
                    $experience->secteur = $request->get("secteur_" . $i);
                    $experience->salaire = $request->get("salaire_" . $i);
                    $experience->employeur = $request->get("employeur_" . $i);
                    $experience->date_debut = $request->get("date_debut_" . $i);
                    $experience->date_fin = $request->get("date_fin_" . $i);
                    $experience->save();
                }
            }
            
            // ADD PROFIL NIVEAU LANGUES //
            if( $total_langues == 0 ) {
                // dd("has no exp");
            } else {
                for ($i=1; $i <= (int)$total_langues; $i++) {
                    $profil_niveau_langue = new ProfilNiveauLangue;
                    $profil_niveau_langue->id_profil = $profil->id_profil;
                    $profil_niveau_langue->id_langue = $request->get("langue_" . $i);
                    $profil_niveau_langue->id_niveau_langue = $request->get("niveau_" . $i);
                    $profil_niveau_langue->save();
                }
            }

            $suivi_offre = new SuiviOffre;
            $suivi_offre->id_offre = $id_offre;
            $suivi_offre->id_profil = $profil->id_profil;
            $suivi_offre->save();

            $offre = Offre::find($id_offre);

            $get_profile = $profil;
            $get_offre = $offre;

            $request->session()->put('success', 'Veuillez ajouter votre CV pour completez votre inscription!');
            return view("profile/validation", compact("get_profile", "get_offre"));
        }
    }

    public function valider_profile(Request $request) {

        $validator = \Validator::make($request->all(), ["cv_file" => "required|mimes:pdf"]);

        if( $validator->fails() ) {
            $messages = $validator->messages();
            return \Redirect::to('/offres/p')->withErrors($validator)->withInput($request->all());
        } else {
            $profil = Profil::find($request->get("id_profile"));
            $offre = Offre::find($request->get("id_offre"));

            $cv_file = $request->file("cv_file");
            $profil->cv_file = $cv_file->hashName();
            $cv_file->storePubliclyAs("CVs/" . $offre->id_offre .  "/", Auth::user()->cin . "_" . $cv_file->getClientOriginalName(), "public");
            // \Storage::disk('local')->put("CVs/" . $offre->id_offre .  "/" . Auth::user()->cin . "_". $cv_file->getClientOriginalName(), $cv_file);
            $profil->save();

            $request->session()->put('success', 'Vous avez été enregistré avec succès!, vous pouvez télécharger le reçu maintenant!');
            return view("profile/recu", compact("profil", "offre"));
        }
    }

    public function telecharger_recu(Request $request) {
        $request->session()->forget('success');
        $profil = Profil::find($request->get("id_profile"));
        $offre = Offre::find($request->get("id_offre"));
        $pdf = PDF::loadView("profile/telecharger", compact("profil", "offre"));
        return $pdf->download("reçu_" . Auth::user()->cin . ".pdf" , array('Attachment'=>0));
    }

    public function telecharger_recu_2($id_offre, $id_profile) {
        $profil = Profil::find($id_profile);
        $offre = Offre::find($id_offre);
        $pdf = PDF::loadView("profile/telecharger", compact("profil", "offre"));
        return $pdf->download("reçu_" . Auth::user()->cin . ".pdf" , array('Attachment'=>0));
    }

    public function annuler_profile(Request $request, $id_profil) {
        $profil = Profil::find($id_profil);
        $profil->delete();

        $new_profil = new Profil;
        $new_profil->id_user = Auth::user()->id;
        $new_profil->id_niveau = "1";
        $new_profil->salaire = "none";
        $new_profil->disponibilite = "none";
        $new_profil->id_etab = "1";
        $new_profil->autre_etablissement = "none";
        $new_profil->id_specialite = "1";
        $new_profil->autre_specialite = "none";
        $new_profil->diplome = "none";
        $new_profil->cv_file = "none";
        $new_profil->save();

        return \Redirect::to("/home")->with('success','Postulation annuler!');
    }
    
    public function afficher_homepage(Request $request) {
        $data = $request->get("a_afficher");

        $list_afficher_r = $request->get("afficher_r_hidden");
        $list_afficher = $request->get("afficher_hidden");

        if( $list_afficher != null ) {
            $offres = Offre::whereIn("id_offre", $list_afficher)->get();
            foreach( $offres as $offre ) {
                $offre->a_afficher = "1";
                $offre->save();
            }
        }
        
        if( $list_afficher_r != null ) {
            $offres_ = Offre::whereIn("id_offre", $list_afficher_r)->get();
            foreach( $offres_ as $offre_ ) {
                $offre_->a_afficher = "0";
                $offre_->save();
            }
        }

        return \Redirect::to("/home")->with('success','Modification bien ajoutée!');
    }

    public function afficher_r_homepage(Request $request) {

        $list_afficher_r = $request->get("afficher_r_hidden");
        $list_afficher = $request->get("afficher_hidden");

        if( $list_afficher != null ) {
            $offres = Offre::whereIn("id_offre", $list_afficher)->get();
            foreach( $offres as $offre ) {
                $offre->a_afficher_r = "1";
                $offre->save();
            }
        }
        
        if( $list_afficher_r != null ) {
            $offres_ = Offre::whereIn("id_offre", $list_afficher_r)->get();
            foreach( $offres_ as $offre_ ) {
                $offre_->a_afficher_r = "0";
                $offre_->save();
            }
        }
        
        return \Redirect::to("/home")->with('success','Modification bien ajoutée!');
    }
}