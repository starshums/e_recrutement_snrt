<?php

namespace App\Http\Middleware;

use Closure;
use App\Offre;

class CheckPosted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id_offre = $request->route()->parameters["id_offre"];
        // dd( $id_offre );
        $offre = Offre::find($id_offre);
        if($offre) {
            if( \Auth::user()->profile->suivi_offres->find($id_offre) ) {
                return redirect('/offres/lister/p');
            }
        } else {
            return redirect('/offres/lister/p');
        }
        
        return $next($request);
    }
}
