<?php

namespace App\Http\Middleware;

use Closure;
use App\Offre;

class CheckOffre
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id_offre = $request->route()->parameters["id_offre"];
        $offre = Offre::find($id_offre);
        if($offre) {
            if( $offre->a_afficher == "0" && \Auth::user()->hasRole("ADMIN") != 1) {
                return redirect('/offres/lister/p');
            }
        } else {
            return redirect('/offres/lister/p');
        }
        return $next($request);
    }
}
