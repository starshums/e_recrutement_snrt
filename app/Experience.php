<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $fillable = [
        "poste",
        "secteur",
        "employeur",
        "date_debut",
        "date_fin"
    ];

    protected $primaryKey = "id_experience";

    public function profil() {
        return $this->belongsTo("App\Profil", "id_profil");
    }
}
