<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cin',
        'nom',
        'prenom',
        'date_naissance',
        'telephone',
        'adresse',
        'ville',
        'pays',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile() {
        return $this->hasOne("App\Profil", "id_user");
    }

    public function roles() {
        return $this->belongsToMany("App\Role", "role_user", "id_user", "id_role");
    }

    public function hasRole($role) {
        return null !== $this->roles()->where("nom", $role)->first();
    }

    public function hasAnyRole($roles) {
        return null !== $this->roles()->whereIn($roles)->first();
    }

    public function authorizeRoles($roles) {
        if( is_array($roles) ) {
            return null !== $this->hasAnyRole($roles);
        }
        return null !== $this->hasRole($roles);
    }
}
