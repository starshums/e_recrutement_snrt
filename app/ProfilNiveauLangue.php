<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilNiveauLangue extends Model
{
    protected $table = "profils_niveau_langues";

    protected $fillable = [
        "id_profil",
        "id_langue",
        "id_niveau_langue"
    ];

    protected $primaryKey = "id_profils_niveau_langue";
    
    public function profil() {
        return $this->belongsTo("App\Profil", "id_profil");
    }

    public function langue() {
        return $this->belongsTo("App\Langue", "id_langue");
    }

    public function niveau_langue() {
        return $this->belongsTo("App\NiveauLangue", "id_niveau_langue");
    }
}
