<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        "nom",
        "description"
    ];

    protected $primaryKey = "id_role";

    public function users() {
        return $this->belongsToMany("App\User", "role_user", "id_role", "id_user");
    }
}
