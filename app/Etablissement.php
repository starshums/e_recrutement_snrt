<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etablissement extends Model
{
    protected $fillable = [
        "nom"
    ];

    protected $primaryKey = "id_etab";

    public function profils() {
        return $this->hasMany("App\Profil", "id_profil");
    }
}
