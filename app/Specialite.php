<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialite extends Model
{
    protected $fillable = [
        "nom"
    ];

    protected $primaryKey = "id_specialite";

    public function profils() {
        return $this->hasMany("App\Profil", "id_profil");
    }
}
