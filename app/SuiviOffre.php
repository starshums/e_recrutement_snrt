<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuiviOffre extends Model
{
    protected $fillable = [
        "id_profil",
        "id_offre"
    ];

    protected $primaryKey = "id_suivi_offre";

    public function profil() {
        return $this->belongsTo("App\Profil", "id_profil");
    }

    public function offre() {
        return $this->belongsTo("App\Offre");
    }
}
