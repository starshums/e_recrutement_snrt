<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Niveau extends Model
{
    protected $fillable = [
        "nom"
    ];

    protected $primaryKey = "id_niveau";

    public function profils() {
        return $this->hasMany("App\Profil", "id_profil");
    }
}
