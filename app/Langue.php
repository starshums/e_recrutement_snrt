<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Langue extends Model
{
    protected $fillable = [
        "nom",
    ];

    protected $primaryKey = "id_langue";
}
