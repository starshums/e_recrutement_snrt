<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer('*', function ($view) {
            $p_path = base_path();
            $main_www_path = str_replace('e_recrutement_snrt', '', $p_path);
            $main_www_path = str_replace('\www', '', $main_www_path);
            $main_www_path = str_replace("\\", '/', $main_www_path);
            // dd( $main_www_path );
            $view->with('main_www_path', $main_www_path);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
}
