<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $fillable = [
        "id_user",
        "id_specialite",
        "id_niveau",
        "id_etab",
        "id_profils_niveau_langue",
        "salaire",
        "disponibilite",
        "autre_etablissement",
        "autre_specialite",
        "diplome",
        "cv_file"
    ];

    protected $primaryKey = "id_profil";

    public function user() {
        return $this->belongsTo("App\User", "id_user");
    }

    public function profil_niveau_langues() {
        return $this->hasMany("App\ProfilNiveauLangue", "id_profil");
    }

    public function specialite() {
        return $this->belongsTo("App\Specialite", "id_specialite");
    }

    public function niveau() {
        return $this->belongsTo("App\Niveau", "id_niveau");
    }

    public function etablissment() {
        return $this->belongsTo("App\Etablissement", "id_etab");
    }

    public function experiences() {
        return $this->hasMany("App\Experience", "id_profil");
    }

    public function suivi_offres() {
        return $this->belongsToMany("App\Offre", "suivi_offres", "id_profil", "id_offre");
    }
}
