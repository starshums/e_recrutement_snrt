<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offre extends Model
{
    protected $fillable = [
        "operation",
        "code_operation",
        "metier",
        "nbr_postes",
        "date_offre",
        "date_delai",
        "fichier_offre",
        "fichier_resultat"
    ];

    protected $primaryKey = "id_offre";

    public function suivi_profil() {
        return $this->belongsToMany("App\Profil", "suivi_offres", "id_offre", "id_profil");
    }

    public function offres_resultats() {
        return $this->hasMany('App\OffreResultat', 'id_offre');
    }
}
