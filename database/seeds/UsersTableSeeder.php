<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'cin' => "ADMIN",
            "nom" => "ADMIN",
            "prenom" => "ADMIN",
            "date_naissance" => "2019/02/12",
            "telephone" => "0000000000",
            "adresse" => "SNRT RABAT",
            "ville" => "RABAT",
            "pays" => "MOROCCO",
            "email" => "snrt.admin@gmail.com",
            "password" => '$2y$10$dn4pfh/DBfav1e3sF9wc8.raoJMHlj3XkRwFG7bNwwSWT/zBf1mzu'
        ]);

        $profil = new App\Profil;
        $profil->id_user = $user->id;
        $profil->id_niveau = "1";
        $profil->salaire = "1";
        $profil->disponibilite = "1";
        $profil->id_etab = "1";
        $profil->autre_etablissement = "1";
        $profil->id_specialite = "1";
        $profil->autre_specialite = "1";
        $profil->diplome = "1";
        $profil->cv_file = "none";
        $profil->save();

        $user->roles()->attach(App\Role::where("nom", "ADMIN")->first()->id_role);
    }
}
