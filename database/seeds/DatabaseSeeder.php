<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguesTableSeeder::class);
        $this->call(NiveauLanguesSeeder::class);
        $this->call(NiveausTableSeeder::class);
        $this->call(EtablissementsTableSeeder::class);
        $this->call(SpecialitesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}