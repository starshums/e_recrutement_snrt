<?php

use Illuminate\Database\Seeder;

class NiveausTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('niveaus')->insert([
            [ 'nom' => "NIVEAU BAC" ],
            [ 'nom' => "BACCALAUREAT" ],
            [ 'nom' => "CQP" ],
            [ 'nom' => "DT" ],
            [ 'nom' => "BTS / DTS / DUT" ],
            [ 'nom' => "LICENCE FONDAMENTAL" ],
            [ 'nom' => "LICENCE PROFESSIONNELLE" ],
            [ 'nom' => "MASTER" ],
            [ 'nom' => "MASTER PROFESSIONNELLE" ],
            [ 'nom' => "INGENIEUR" ],
            [ 'nom' => "DOCTORAT" ]
        ]);
    }
}
