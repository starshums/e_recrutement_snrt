<?php

use Illuminate\Database\Seeder;

class LanguesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('langues')->insert([
            [ 'nom' => "Arabe" ],
            [ 'nom' => "Amazigh" ],
            [ 'nom' => "Français" ],
            [ 'nom' => "Anglais" ],
            [ 'nom' => "Espagnol" ]
        ]);
    }
}
