<?php

use Illuminate\Database\Seeder;

class NiveauLanguesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('niveau_langues')->insert([
            [ 'niveau' => "Notions" ],
            [ 'niveau' => "Intermediaire" ],
            [ 'niveau' => "Avencé" ],
            [ 'niveau' => "Excelent" ]
        ]);
    }
}
