<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuivioffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suivi_offres', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id_suivi_offre');
            $table->integer("id_profil")->unsigned();
            $table->foreign("id_profil")->references("id_profil")->on('profils')->onDelete("cascade")->onUpdate("cascade");
            $table->integer("id_offre")->unsigned();
            $table->foreign("id_offre")->references("id_offre")->on('offres')->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suivi_offres');
    }
}
