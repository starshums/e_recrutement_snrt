<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profils', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id_profil');
            $table->integer("id_user")->unsigned();
            $table->integer("id_specialite")->unsigned();
            $table->integer("id_niveau")->unsigned();
            $table->integer("id_etab")->unsigned();
            $table->foreign("id_user")->references("id")->on("users")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("id_specialite")->references("id_specialite")->on("specialites")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("id_niveau")->references("id_niveau")->on("niveaus")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("id_etab")->references("id_etab")->on("etablissements")->onDelete("cascade")->onUpdate("cascade");
            $table->string("salaire");
            $table->string("disponibilite");
            $table->string("autre_etablissement");
            $table->string("autre_specialite");
            $table->string("diplome");
            $table->string("cv_file");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profils');
    }
}
