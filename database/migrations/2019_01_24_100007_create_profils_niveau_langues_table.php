<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilsNiveauLanguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profils_niveau_langues', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id_profils_niveau_langue');
            $table->integer("id_profil")->unsigned();
            $table->integer("id_langue")->unsigned();
            $table->integer("id_niveau_langue")->unsigned();
            $table->foreign("id_profil")->references("id_profil")->on('profils')->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("id_langue")->references("id_langue")->on("langues")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("id_niveau_langue")->references("id_niveau_langue")->on("niveau_langues")->onDelete("cascade")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profils_niveau_langues');
    }
}
