<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offres', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id_offre');
            $table->string("operation");
            $table->string("code_operation");
            $table->string("metier")->nullable();
            $table->string("nbr_postes")->nullable();
            $table->date("date_offre")->nullable();
            $table->date("date_delai")->nullable();
            $table->string("fichier_offre")->nullable();
            $table->string("a_afficher")->nullable();
            $table->string("a_afficher_r")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offres');
    }
}
