<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffresResultatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offres_resultats', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id_offres_resultat');
            $table->integer("id_offre")->unsigned();
            $table->foreign("id_offre")->references("id_offre")->on("offres")->onDelete("cascade")->onUpdate("cascade");
            $table->string("fichier_resultat");
            $table->string("description")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offres_resultats');
    }
}
