<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id_experience');
            $table->integer("id_profil")->unsigned();
            $table->foreign("id_profil")->references("id_profil")->on('profils')->onDelete("cascade")->onUpdate("cascade");
            $table->string("poste");
            $table->string("secteur");
            $table->string("employeur");
            $table->string("salaire");
            $table->date("date_debut");
            $table->date("date_fin");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
}
