<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("home_test", function() {
    $offres = App\Offre::all()->where("a_afficher", "=", "1");
    $offres_r = App\Offre::all()->where("fichier_resultat", "!=", "none")->where("a_afficher_r", "=", "1");
    return view("home_test", compact("offres", "offres_r"));
});

Auth::routes();

// ## ROUTES THAT REQUIRE AUTHORIZATION ## //
$router->group(['middleware' => 'auth'], function() {
    
    // ## HOME ROUTE ## // ## // ## // ## // ## // ## //
    Route::get('/home', 'HomeController@index')->name('home');
    
    // ## OFFRES ## // ## // ## // ## // ## // ## // ## //
    Route::post('/offres/ajouter', "OffresController@ajouter")->name('offres.ajouter');

    Route::get("/offres/ajouter", function() {
        return view("offres/ajouter");
    })->middleware("CheckRoles");

    Route::get("/offres/details/{id_offre}", function($id_offre) {
        $offre = App\Offre::find($id_offre);
        return view("offres/details", compact("offre"));
    })->middleware(["CheckOffre"]);

    Route::get("/offres/p", function() {
        $offres = App\Offre::all();
        return view('offres/index', compact("offres"));
    })->middleware("CheckRoles");

    Route::get("/offres/r", function() {
        $offres = App\Offre::all();
        return view('offres/index', compact("offres"));
    })->middleware("CheckRoles");

    Route::get("/offres/afficher", function() {
        $offres =  App\Offre::orderBy('a_afficher', 'ASC')->get();
        return view('offres/afficher', compact("offres"));
    })->middleware("CheckRoles");

    Route::get("/offres/uncheck/{id_offre}", function($id_offre) {
        $offre = App\Offre::find($id_offre);
        $offre->a_afficher = "0";
        $offre->save();
        $offres =  App\Offre::orderBy('a_afficher', 'ASC')->get();
        return view('offres/afficher', compact("offres"));
    })->middleware("CheckRoles");

    Route::get("/offres/afficher/r", function() {
        $offres = App\Offre::all();
        return view('offres/afficher_r', compact("offres"));
    })->middleware("CheckRoles");

    Route::get("/offres/lister/p", function() {
        $offres = App\Offre::all()->where("a_afficher", "=", "1");
        return view('offres/offres_p', compact("offres"));
    });

    Route::get("/offres/lister/r", function() {
        $offres_r = App\Offre::all()->where("a_afficher_r", "=", "1");
        return view('offres/offres_r', compact("offres_r"));
    });

    Route::post("/offres/afficher", "OffresController@afficher_homepage")->middleware("CheckRoles")->name("offres.afficher_homepage");
    Route::post("/offres/afficher/r", "OffresController@afficher_r_homepage")->middleware("CheckRoles")->name("offres.afficher_r_homepage");
    Route::post("/offres/modifier_affichage_homepage", "OffresController@modifier_afficher_homepage")->middleware("CheckRoles")->name("offres.modifier_afficher_homepage");
    Route::post("/offres/postuler/{id_offre}", "OffresController@postuler")->name("offres.postuler");
    
    Route::get("/offres/postuler/{id_offre}", function($id_offre) {
        $offre = App\Offre::find($id_offre);
        $etabs = App\Etablissement::pluck("nom", "id_etab");
        $specialites = App\Specialite::pluck("nom", "id_specialite");
        $niveau_formations = App\Niveau::pluck("nom", "id_niveau");
        $niveau_langues = App\NiveauLangue::pluck("niveau", "id_niveau_langue");
        $langues = App\Langue::pluck("nom", "id_langue");
        return view("offres/postuler", compact("offre", "etabs", "specialites", "niveau_formations", "niveau_langues", "langues"));
    })->middleware("CheckPosted");

    Route::post("/offres/ajouter/fichier/resultat", "OffresController@ajouter_fichier_resultat")->name("offre.ajouter.fichier_resultat")->middleware("CheckRoles");
    // ## PROFILES ## // ## // ## // ## // ## // ## // ## //
    Route::get("/profile/mon_profile", function() {
        $user = App\User::find(\Auth::user()->id);
        $profil = App\Profil::where( "id_user", "=", Auth::user()->id )->first();
        $etabs = App\Etablissement::pluck("nom", "id_etab");
        $specialites = App\Specialite::pluck("nom", "id_specialite");
        $niveau_formations = App\Niveau::pluck("nom", "id_niveau");
        $niveau_langues = App\NiveauLangue::pluck("niveau", "id_niveau_langue");
        $langues = App\Langue::pluck("nom", "id_langue");
        return view("profile/profile", compact("user", "profil", "etabs", "specialites", "niveau_formations", "niveau_langues", "langues"));
    });

    Route::post("/profile/modifier", "HomeController@modifier")->name('profile.modifier');
    Route::get("/profile/modifier", function() {
        return view("profile/modifier");
    });

    Route::post("/profile/modifier/diplomes", "HomeController@modifier_diplomes")->name('profile.modifier_diplomes');
    Route::post("/profile/modifier/status", "HomeController@modifier_status")->name('profile.modifier_status');
    Route::post("/profile/modifier/experience", "HomeController@modifier_experiences")->name('profile.modifier_experiences');
    Route::post("/profile/modifier/langue", "HomeController@modifier_langues")->name('profile.modifier_langues');
    Route::get("/profile/validation", function() {
        return view("profile.validation");
    });

    Route::post("/profile/validation", "OffresController@valider_profile")->name("profile.validation");
    Route::get("/profile/annuler/{id_profil}", "OffresController@annuler_profile")->name("profile.annuler");
    Route::post("/telecharger", "OffresController@telecharger_recu")->name("profile.telecharger");
    Route::get("/telecharger/recu/{id_profile}/{id_offre}", "OffresController@telecharger_recu_2")->middleware("CheckPosted");
});